<?php

class Basket {

    private $basketItems;
    private $basketPrice;

    //private $userAddresses

    public function __construct() {

        $this->basketPrice = 0.0;
        $this->basketItems = array();
    }

    public function getPrice() {
        return $this->basketPrice;
    }

    public function getItems() {
        return $this->basketItems;
    }
    public function setItems($items){
        $this->basketItems=$items;
        $this->updateSum();
    }
   
    public function addItem($item, $quantity=1) {
        if (array_key_exists($item->getId(), $this->basketItems))
            $this->basketItems[$item->getId()]->addQuantity($quantity);
        $this->basketItems[$item->getId()] = $item;
        $this->basketItems[$item->getId()]->setQuantity($quantity);
        $this->updateSum();
    }
    
    public function removeItem($item){
        unset($this->basketItems[$item->getId()]);
        $this->updateSum();
    }
    public function changeQuantity($item, $quantity){
        if (array_key_exists($item->getId(), $this->basketItems))
            $this->basketItems[$item->getId()]->addQuantity($quantity);
        if ($this->basketItems[$item->getId()]->getQuantity()==0)
            $this->removeItem($item);
        $this->updateSum();
    }
    
    private function updateSum() {
        $sum = 0;
        foreach ($this->basketItems as $itemid => $item) {
            $sum+=$item->getPrice() * $item->getQuantity();
        }
        $this->basketPrice=$sum;
    }
    
}

?>
