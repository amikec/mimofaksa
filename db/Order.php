<?php

include_once 'Basket.php';

class Order {

    private $orderID;
    private $orderBasket;
    private $orderStatus;
    private $orderDate;
    private $orderDateComplete;
    private $orderUserID;
    private $orderAddressID;

    public function __construct($ID, $items, $status, $date, $datecomplete, $userid, $addressid) {

        $this->orderID = $ID;
        $this->orderBasket = new Basket();
        $this->orderBasket->setItems($items);
        $this->orderStatus = $status;
        $this->orderDate = $date;
        $this->orderDateComplete = $datecomplete;
        $this->orderUserID = $userid;
        $this->orderAddressID = $addressid;
    }

    public function getID() {
        return $this->orderID;
    }

    public function getBasket() {
        return $this->orderBasket;
    }

    public function getItems() {
        return $this->orderBasket->getItems();
    }
    public function getStatus() {
        return $this->orderStatus;
    }
    public function getPrice() {
        return $this->orderBasket->getPrice();
    }

    public function getDate() {
        return $this->orderDate;
    }

    public function getDateComplete() {
        return $this->orderDateComplete;
    }

    public function getUserID() {
        return $this->orderUserID;
    }

    public function getAddressID() {
        return $this->orderAddressID;
    }

    public function setBasket($basket) {
        $this->orderBasket = $basket;
    }

    public function setItems($items) {
        $this->orderBasket->setItems($items);
    }

    public function setDate($date) {
        $this->orderDate = $date;
    }

    public function setDateCompleted($datecompleted) {
        $this->orderDateComplete = $datecompleted;
    }

    public function setUserID($userID) {
        $this->orderUserID = $userID;
    }

    public function setAddressID($addressID) {
        $this->orderAddressID = $addressID;
    }
      public function setStatus($status) {
        $this->orderStatus=$status;
    }
}

?>
