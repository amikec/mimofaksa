<?php

class Address {

    private $addressID;
    private $addressStreet;
    private $addressCity;
    private $addressPostCode;
    private $addressCountry;
    private $addressUserID;

    public function __construct($ID, $street, $city, $postcode, $country, $userid) {
        $this->addressID = $ID;
        $this->addressStreet = $street;
        $this->addressCity = $city;
        $this->addressPostCode = $postcode;
        $this->addressCountry = $country;
        $this->addressUserID = $userid;
    }

    public function getID() {
        return $this->addressID;
    }

    public function getStreet() {
        return $this->addressStreet;
    }

    public function getCity() {
        return $this->addressCity;
    }

    public function getCountry() {
        return $this->addressCountry;
    }

    public function getPostCode() {
        return $this->addressPostCode;
    }

    public function getUserID() {
        return $this->addressUserID;
    }

    public function setStreet($street) {
        $this->addressStreet = $street;
    }

    public function setCity($city) {
        $this->addressCity = $city;
    }

    public function setPostCode($postcode) {
        $this->addressPostCode = $postcode;
    }

    public function setCountry($country) {
        $this->addressCountry = $country;
    }

    public function setUserID($id) {
        $this->addressUserID = $id;
    }

    public function toString() {
        return $this->addressID . ", " . $this->addressPostCode . " " . $this->addressCity . ", " . $this->addressCountry;
    }

}

?>
