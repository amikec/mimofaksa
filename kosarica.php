<?php

require_once 'db/dbConnection.php';

if($anonymous){
    header("Location: index.php?page=login");
    exit;
}

if (!isset($_SERVER["HTTPS"])) {
    $url = "https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
    header("Location: " . $url);
}

$step = 1;

if (isset($_GET['step'])) {
    $step = $_GET['step'];
}

$db = new dbConnection();
$user = unserialize($_SESSION['user']);
$basket = new Basket();

if (isset($_SESSION['basket'])) {
    $basket = unserialize($_SESSION['basket']);
}
    

if (isset($_GET['action'])) {

    if (isset($_GET['id'])) {

        $items = $basket->getItems();
        $item = $items[$_GET['id']];


        if ($_GET['action'] == "reduce") {
            $tmp = $item->getQuantity();
            $basket->changeQuantity($item, -1);
        } else if ($_GET['action'] == "add") {
            $tmp = $item->getQuantity();
            $basket->changeQuantity($item, 1);
        } else if ($_GET['action'] == "remove") {
            $basket->removeItem($item);
        }
    }

    if ($_GET['action'] == "confirm") {

        $address = unserialize($_SESSION['address']);
        $db->createOrder($user, $basket, $address);
        $basket = new Basket();
        $_SESSION['basket'] = serialize($basket);
        header("Location: index.php?page=basket&step=4");
        exit;
    }

    $_SESSION['basket'] = serialize($basket);
}
?>
<div class="kosarica"> 
    <table>
<?php if ($step == 1) { ?>


            <tr>
                <th id='drugi'>Artikel</th><th>Kolicina</th><th>Cena</th><th colspan="3">Spremeni</th>
            </tr>
    <?php
    foreach ($basket->getItems() as $itemId => $item) {
        echo "<tr>
            <td id='drugi'>" . $item->getName() . "</td>
            <td>" . $item->getQuantity() . "</td>
            <td>" . $item->getPrice() . "</td>
            <td><a id='btn' href='index.php?page=basket&action=reduce&id=" . $itemId . "'>-</a></td>
            <td><a id='btn' href='index.php?page=basket&action=add&id=" . $itemId . "'>+</a></td>
            <td><a id='btn' href='index.php?page=basket&action=remove&id=" . $itemId . "'>x</a></td>
            </tr>";
    }
    if($basket->getItems() != null)
        echo "<tr><td><a id='btn' href='index.php?page=basket&step=2'>Naprej</a></td></tr>";
} else if ($step == 2) {
    $adresses = $db->getAddressesByUserID($user->getID());
    echo "<tr><td>Izberite naslov za prejem</td></tr>";
    echo "<tr><td><a href='index.php?page=address' >Dodaj</a></td></tr>";
    foreach ($adresses as $adress) {
        echo "<tr><td>Ulica: " . $adress->getStreet() . "</td></tr>";
        echo "<tr><td>Mesto: " . $adress->getCity() . "</td></tr>";
        echo "<tr><td>Posta: " . $adress->getPostcode() . "</td></tr>";
        echo "<tr><td>Drzava: " . $adress->getCountry() . "</td></tr>";
        echo "<tr><td><a id='btn' href='index.php?page=basket&action=address&adid=" . $adress->getID() . "&step=3'>Izberi</a></td></tr>";
        echo "<tr><td><hr></td></6>";
    }
    //echo "<tr><td><a id='btn' href='index.php?page=basket&step=3'>Naprej</a></td></tr>";
} else if ($step == 3) {
    $address = $db->getAddressById($_GET['adid']);
    $_SESSION['address'] = serialize($address);
    //izpis prednaročila in potrdilo nakupa

    echo "<tr><th>Naziv</th><th>Kolicina</th><th>Cena</th></tr>";
    $basket = unserialize($_SESSION['basket']);
    foreach ($basket->getItems() as $itemID => $item) {
        echo"<tr><td>" . $item->getName() . "</td><td>" . $item->getQuantity() . "</td><td>" . $item->getQuantity() * $item->getPrice() . " €</td></tr>";
    }
    echo "<tr><td colspan='2'>Skupno:</td><td>" . $basket->getPrice() . "</td></tr>";
    echo "<tr><hr></tr>";
    echo "<tr><td><a href='index.php?page=basket&action=confirm'>Potrdi naročilo</a></td></tr>";
} else if ($step == 4) {
    echo "<tr><td>Najlepsa hvala za nakup, vase narocilo je bilo poslano v obdelavo!</td></tr>";
}
?>
    </table>
</div>



