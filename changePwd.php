<?php

require_once 'HTML/QuickForm2.php';

if($anonymous){
    header("Location: index.php?page=login");
    exit;
}

if (!isset($_SERVER["HTTPS"])) {
    $url = "https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
    header("Location: " . $url);
}


$form = new HTML_QuickForm2('pwdUpdate', 'POST', array('action' => 'index.php?page=pwd'));

$fs = $form->addFieldset('Geslo');
$fs->setLabel('Geslo');

$staro = $fs->addElement('password', 'oldPwd', array('size' => 20))
        ->setLabel('Staro geslo:');
$staro->addRule('required', 'Vnesi staro geslo.');
$staro->addRule('regex','Napačen format.', '/^[A-Za-z _0-9]+$/');

$novo = $fs->addElement('password', 'newPwd', array('size' => 20))
        ->setLabel('Novo geslo:');
$novo->addRule('regex','Napačen format.', '/^[A-Za-z _0-9]+$/');

$novo2 = $fs->addElement('password', 'newPwd2', array('size' => 20))
        ->setLabel('Ponovi:');
$novo2->addRule('regex','Napačen format.', '/^[A-Za-z _0-9]+$/');

$fs->addElement('submit', null, array('value' => 'Spremeni'));


if ($form->validate()) {
    if (strcmp($_POST['newPwd'], $_POST['newPwd2']) == 0) {
        $db = new dbConnection();
        $user = unserialize($_SESSION['user']);
        $tmp = $db->getUserByLogin($user->getEmail(), $_POST['oldPwd']) ;
        if ($tmp!= null && $tmp->getID() == $user->getID()) {
            $db->updateUserPassword($user, $_POST['newPwd']);
            header("Location: index.php?page=introduction");
            exit;
        } else {
            echo $form;
            echo "Geslo ni pravilno";
        }
    } else {
        echo $form;
    }
} else {
    echo $form;
}

