<?php
require_once 'HTML/QuickForm2.php';

if(!isset($_SESSION['user'])){
    header("Location: index.php?page=login");
    exit;
}

if (!isset($_SERVER["HTTPS"])) {
    $url = "https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
    header("Location: " . $url);
}


$form = new HTML_QuickForm2('dodajNaslov','POST',array('action' => 'index.php?page=address'));

$fs = $form->addFieldset('naslov');
$fs->setLabel('Nov naslov');

$city = $fs->addElement('text', 'city', array('size' => 20))
        ->setLabel('Mesto:');
$city->addRule('required', 'Vnesi mesto.');
$city->addRule('regex','Napačen format.', '/^[A-Za-z ]+$/');

$street = $fs->addElement('text', 'street', array('size' => 20))
        ->setLabel('Ulica:');
$street->addRule('required', 'Vnesi ulico.');
$street->addRule('regex','Napačen format.', '/^[A-Za-z0-9 -]+$/');

$postcode = $fs->addElement('text', 'postcode', array('size' => 20))
        ->setLabel('Postna stevilka:');
$postcode->addRule('required', 'Vnesi poštno številko.');
$postcode->addRule('regex','Napačen format.', '/^[0-9]{4}$/');

$country = $fs->addElement('text', 'country', array('size' => 20))
        ->setLabel('Država:');
$country->addRule('required', 'Vnesi državo.');
$country->addRule('regex','Napačen format.', '/^[A-Za-z ]+$/');


$fs->addElement('submit', null, array('value' => 'Shrani'));

$db = new dbConnection();

if($form->validate()){
    $user = unserialize($_SESSION['user']);
    $db->createAddress($user, $_POST['street'], $_POST['city'], $_POST['postcode'], $_POST['country']);
    header('Location: index.php?page=basket&step=2');
    exit;
}else{
    echo $form;
}


?>