<?php

$db = new dbConnection();
$items = array();
$ratingMsg = "";
$ratingID = 0;

require_once 'HTML/QuickForm2.php';

$action = "";
if (isset($_GET['action']))
    $action = $_GET['action'];

$form = new HTML_QuickForm2('addItem', 'POST', array('action' => 'index.php?page=editArt'));

$fs = $form->addFieldset('artikel');
$fs->setLabel('Podatki artikla:');

$id = 0;
if (isset($_GET['id']))
    $id = $_GET['id'];
else if(isset($_POST['id']))
    $id = $_POST['id'];

$id2 = $fs->addElement('hidden', 'id', array('size' => 20));
$tmp = $fs->addElement('hidden', 'edit', array('size' => 20));

$name = $fs->addElement('text', 'name', array('size' => 20))
        ->setLabel('Naziv:');
$name->addRule('required', 'Vnesi naziv.');
$name->addRule('regex', 'Napacen format.', '/^[A-Za-z0-9 _-]+$/');

$description = $fs->addElement('textarea', 'description', array('size' => 20))
        ->setLabel('Opis:');
$description->addRule('required', 'Vnesi opis.');

$category = $fs->addElement('text', 'category', array('size' => 20))
        ->setLabel('Kategorija:');
$category->addRule('required', 'Vnesi kategorijo');

$price = $fs->addElement('text', 'price', array('size' => 20))
        ->setLabel('Cena:');
$price->addRule('required', 'Vnesi ceno');
$price->addRule('regex', 'V obliki: 5.90', '/^[0-9.]+$/');

$fs->addElement('submit', null, array('value' => 'Dodaj'));

echo "<form method='post' action='index.php?page=editArt'>
     <input type='hidden' name='action' value='search' />
     <table class='searchTable'><tr>
         <td>
            Kategorija :
            <select name='category'>
                <option value=''>Vse</option>
                <option value='računalništvo'>Računalništvo</option>
                <option value='foto'>Foto</option>
                <option value='avdio-video'>Avdio-Video</option>
                <option value='gospodinjstvo'>Gospodinjstvo</option>
            </select>
         </td>
         <td>
            Kriterij:
            <select name='criteria'>
                <option value='price'>cena</option>
                <option value='rating'>ocena</option>
            </select>
         </td>
         <td>
            Smer:
            <select name='descending'>
                <option value='false'>Naraščajoče</option>
                <option value='true'>Padajoče</option>
            </select>
         </td>
         <td>Min: <input type='text' name='min' size='10' /></td>
         <td>Max: <input type='text' name='max' size='10' /></td>
         <td><input type='submit' value='Izpiši'</td>
     </tr></table>
     </form>";

if ($action == "edit" || isset($_POST['edit'])) {
    if ($form->validate()) {
        $item = $db->getItemByID($_POST['id']);
        echo "TEST" . isset($_POST['id']);
        echo "TEST" . $item->getName();
        $item->setName($_POST['name']);
        $item->setPrice($_POST['price']);
        $item->setCategory($_POST['category']);
        $item->setDescription($_POST['description']);
        echo "TEST" . $item->getName();
        $db->updateItem($item);
    } else {
        $item = $db->getItemByID($_GET['id']);
        $form->addDataSource(new HTML_QuickForm2_DataSource_Array(array(
                    'name' => $item->getName(),
                    'price' => $item->getPrice(),
                    'category' => $item->getCategory(),
                    'description' => $item->getDescription(),
                    'id' => $id,
                    'edit' => "1")));
        echo $form;
    }
}

if ($action == "changeActive") {
    $item = $db->getItemByID($_GET['id']);
    $item->setActive($_GET['active']);
    $db->updateItem($item);
}

if ($action == 'search') {
    $desc = false;
    if ($_POST['descending'] == "true")
        $desc = true;
    $criteria = $_POST['criteria'];
    $min = $_POST['min'];
    $max = $_POST['max'];
    $category = $_POST['category'];

    if ($criteria == "price") {
        if ($min == null)
            $min = 0;
        if ($max == null)
            $max = 900000;
        $items = $db->getItemsByPrice($min, $max, $desc, $category);
    }else {
        if ($min == null)
            $min = 0;
        if ($max == null)
            $max = 5;
        $items = $db->getItemsByRating($min, $max, $desc, $category);
    }
}else if (isset($_GET['category'])) {
    $category = $_GET['category'];
    $items = $db->getItemsByCategory($category);
} else if (isset($_POST['searchTerm'])) {
    $items = $db->findItems($_POST['searchTerm']);
} else {
    $items = $db->getItems();
}

if ($action != "edit") {


    echo "<div class ='kosarica'><table >";

    foreach ($items as $itemID => $item) {
        if ($item->getRatingN() == 0)
            $rating = "/";
        else
            $rating = number_format(floatval(floatval($item->getRatingSum()) / floatval($item->getRatingN())), 1, '.', '');

        $slika = "../db/slike/" . $item->getPicture();

        $msg = "";

        if ($ratingID == $item->getID())
            $msg = $ratingMsg;

        echo "
        <tr><th id='prvi'>Naziv: </th><th>" . $item->getName() . "</th></tr>
        <tr><td id='prvi'>Kategorija: </td><td>" . $item->getCategory() . "</td></tr>
        <tr><td id='prvi'></td><td><img src='" . $slika . "' /></td></tr>
        <tr><td id='prvi'>Opis: </td><td>" . $item->getDescription() . "</td></tr>
        <tr><td id='prvi'>Cena: </td><td>" . $item->getPrice() . " €</td></tr>";

        if ($item->getActive() == 0)
            echo "<tr><td id='prvi'><a id='btn' href='index.php?page=editArt&action=changeActive&id=" . $item->getID() . "&active=1'>Aktiviraj</a></td>";
        else
            echo "<td id='prvi'><a id='btn' href='index.php?page=editArt&action=changeActive&id=" . $item->getID() . "&active=0'>Deaktiviraj</a></td>";
        echo "<td id='prvi'><a id='btn' href='index.php?page=editArt&action=edit&id=" . $item->getID() . "'>Uredi</a></td></tr>";
    }

    echo "</table>
</div>";
}
?>
