<?php
require_once 'HTML/QuickForm2.php';


$form = new HTML_QuickForm2('addItem','POST',array('action' => 'index.php?page=newItem'));

$fs = $form->addFieldset('artikel');
$fs->setLabel('Podatki artikla:');

$name = $fs->addElement('text', 'name', array('size' => 20))
        ->setLabel('Naziv:');
$name->addRule('required', 'Vnesi naziv.');
$name->addRule('regex', 'Napacen format.', '/^[A-Za-z0-9 _-]+$/');

$description = $fs->addElement('textarea', 'description', array('size' => 20))
        ->setLabel('Opis:');
$description->addRule('required', 'Vnesi opis.');

$category = $fs->addElement('text', 'category', array('size' => 20))
        ->setLabel('Kategorija:');
$category->addRule('required', 'Vnesi kategorijo');

$price = $fs->addElement('text', 'price', array('size' => 20))
        ->setLabel('Cena:');
$price->addRule('required', 'Vnesi ceno');
$price->addRule('regex', 'V obliki: 5.90', '/^[0-9.]+$/');

$fs->addElement('radio', 'active', array('value' => '0'), array('content' => 'Neaktiven')); 
$fs->addElement('radio', 'active', array('value' => '1'), array('content' => 'Aktiven')); 

$fs->addElement('submit', null, array('value' => 'Dodaj'));


$db = new dbConnection();

if($form->validate()){
    $db->createItem($_POST['name'], $_POST['description'], $_POST['category'], $_POST['price'], $_POST['active']);
    echo "Artikel je bil uspešno dodan.";
}else{
    echo $form;
}

?>
