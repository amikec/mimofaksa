<?php
require_once 'HTML/QuickForm2.php';

$form = new HTML_QuickForm2('pwdUpdate','POST',array('action' => 'index.php?page=pwd'));
 
$fs = $form->addFieldset('Geslo');
$fs->setLabel('Geslo');

$staro = $fs->addElement('password', 'oldPwd', array('size' => 20))
        ->setLabel('Staro geslo:');
$staro->addRule('required', 'Vnesi staro geslo.');

$novo = $fs->addElement('password', 'newPwd', array('size' => 20))
        ->setLabel('Novo geslo:');

$novo2 = $fs->addElement('password', 'newPwd2', array('size' => 20))
        ->setLabel('Ponovi:');

$fs->addElement('submit', null, array('value' => 'Spremeni'));

if($form->validate()){
    if(strcmp($_POST['newPwd'], $_POST['newPwd2']) == 0){
        $db = new dbConnection();
        $user = unserialize($_SESSION['user']);
        $tmp = $db->getUserByLogin($user->getEmail(), $_POST['oldPwd']);
        $db->updateUserPassword($user, $_POST['newPwd']);
        
        header("Location: index.php?page=processOrders");
        exit;
        
    }else{
        echo $form;
    }
}else{
    echo $form;
}

