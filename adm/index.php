<?php 
    session_start();
    
    require_once '../db/Basket.php';
    require_once '../db/Item.php';
    require_once '../db/User.php';
    require_once '../db/dbConnection.php';
    require_once '../db/Address.php';
    
    if(!isset($_SESSION['user'])){
        header("Location: ../index.php?page=login");
        exit;
    }
    
    $db = new dbConnection();
    
    $page = "";
    
    $db = new dbConnection();

    if(isset($_GET['page'])){
        $page = $_GET['page'];
    }

?>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link  rel="stylesheet" href="../style.css" type="text/css" />
        <title></title>
    </head>
    <body>
        <table>
            <!-- header -->
            <tr>
                <td class="headerMain" colspan="2">
                    <?php //include 'header.php'
                    ?> 

                </td>
            </tr>
            <!-- center content -->
            <tr>
                <td class ="leftColumn" >
                    <table>
                        <tr>
                            <td><a href="index.php?page=customer">Dodaj/spremeni stranko</a></td>
                        </tr>
                        <tr>
                            <td><a href="index.php?page=salesman">Dodaj/spremeni prodajalca</a></td>
                        </tr>
                        <tr>
                            <td><a href="index.php?page=changePwd">Spremeni geslo</a></td>
                        </tr>    
                            <td><a href="index.php?page=main">Glavno okno</a></td>
                        <tr>    
                            <td><a href="../index.php?page=introduction">Spletna trgovina</a></td>
                        </tr>
                        <tr><td><a href="index.php?page=logout">Odjava</a></td></tr>
                    </table>
                </td>
                <td class ="content" >
                    <?php 
                        switch($page){
                            case "logout":
                                session_destroy();  
                                header("Location: ../index.php");
                                exit;
                                break;
                            case "customer":
                                include 'editCustomer.php';
                                break;
                            case "salesman":
                                include 'editSalesman.php';
                                break;
                            case "changePwd";
                                include 'changePwd.php';
                                break;
                            default:
                                include 'main.php';
                                break;
                        }
                    ?>
                </td>
            </tr>
            <!-- footer -->
            <tr>
                <td class="kfooterMain" colspan="2">
                    <?php //include 'footer.php'
                    ?> 
                </td>
            </tr>
        </table>
    </body>
</html>
