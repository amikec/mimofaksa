<?php

if($anonymous){
    header("Location: index.php?page=login");
    exit;
}

if (!isset($_SERVER["HTTPS"])) {
    $url = "https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
    header("Location: " . $url);
}


$db = new dbConnection();
$user = unserialize($_SESSION['user']);
$orders = $db->getOrdersByUserID($user->getID());
?>
<div class="zgodovina">

    <?php
    if ($orders != null) {
        foreach ($orders as $order) {
            echo "<p><table class='purchase'>";
            $items = $order->getItems();
            echo "<tr><td>Datum oddano: " . $order->getDate() . "</td></tr>";
            echo "<tr><td>Status: ";
                    $status = $order->getStatus();
                    if($status == -1)
                        echo "stornirano";
                    else if($status == 0)
                        echo "v obdelavi";
                    else if($status == -2)
                        echo "stornirano";
                    else
                        echo "potrjeno (". $order->getDateComplete()  .")";
                    echo "</td></tr>";
            echo "<tr><td>Cena:" . $order->getPrice() . "</td></tr>";
            foreach ($items as $itemID => $item) {
                echo "
                    <tr><th id='prvi'>Naziv: </th><th>" . $item->getName() . "</th></tr>
                    <tr><td id='prvi'>Kategorija: </td><td>" . $item->getCategory() . "</td></tr>
                    <tr><td id='prvi'>Opis: </td><td>" . $item->getDescription() . "</td></tr>
                    <tr><td id='prvi'>Cena: </td><td>" . $item->getPrice() . "</td> </tr>";
                    
            }
           echo "</table></p><p><hr /></p>";
        }
    }
    ?>

</div>