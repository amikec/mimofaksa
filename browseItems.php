<?php

require_once 'db/dbConnection.php';
require_once 'db/Item.php';

$db = new dbConnection();
$items = array();
$ratingMsg = "";
$ratingID = 0;

echo "<form method='post' action='index.php?page=browse'>
     <input type='hidden' name='action' value='search' />
     <table class='searchTable'><tr>
         <td>
            Kategorija :
            <select name='category'>
                <option value=''>Vse</option>
                <option value='računalništvo'>Računalništvo</option>
                <option value='foto'>Foto</option>
                <option value='avdio-video'>Avdio-Video</option>
                <option value='gospodinjstvo'>Gospodinjstvo</option>
            </select>
         </td>
         <td>
            Kriterij:
            <select name='criteria'>
                <option value='price'>cena</option>
                <option value='rating'>ocena</option>
            </select>
         </td>
         <td>
            Smer:
            <select name='descending'>
                <option value='false'>Naraščajoče</option>
                <option value='true'>Padajoče</option>
            </select>
         </td>
         <td>Min: <input type='text' name='min' size='10' /></td>
         <td>Max: <input type='text' name='max' size='10' /></td>
         <td><input type='submit' value='Izpiši'</td>
     </tr></table>
     </form>";

if(isset($_GET['action']) && $_GET['action'] == "add"){
    $basket = unserialize($_SESSION['basket']);
    $item = $db->getItemByID($_GET['id']);
    $basket->addItem($item);
    $_SESSION['basket'] = serialize($basket);
}

if(isset($_GET['action']) && $_GET['action'] == 'rate'){
    $ratingID = $_POST['productId'];
    if(isset($_COOKIE['hasVoted']) && $_COOKIE['hasVoted'] == $ratingID){
        $ratingMsg = "Za ta izdelek ste že glasovali.";
    }else{
        $ocena = $_POST['rating'];
        $id = $_POST['productId'];

        $item = $db->getItemByID($id);
        $db->rateItem($id, $ocena);

        $ratingMsg = "Vaš glas je oddan.";
    }
}

if(isset($_POST['action']) && $_POST['action'] == 'search'){
    $desc = false;
    if($_POST['descending'] == "true")
        $desc = true;
    $criteria = $_POST['criteria'];
    $min = $_POST['min'];
    $max = $_POST['max'];
    $category = $_POST['category'];
    
    if($criteria == "price"){
        if($min == null) $min = 0;
        if($max == null) $max = 900000;
        $items = $db->getItemsByPrice($min, $max, $desc, $category);
    }else{
        if($min == null) $min = 0;
        if($max == null) $max = 5;
        $items = $db->getItemsByRating($min, $max, $desc, $category);
    }
}else if(isset($_GET['category'])){
    $category = $_GET['category'];
    $items = $db->getItemsByCategory($category);
}else if(isset($_POST['searchTerm'])){
    $items = $db->findItems($_POST['searchTerm']);
            
}else{
    $items = $db->getItems();
}

?>
<div class ="kosarica">
<table>
    <?php
    if($items!=null){
    foreach($items as $itemID=>$item){
        if($item->getRatingN()==0)
            $rating = "/";
        else
             $rating = number_format(floatval(floatval($item->getRatingSum()) / floatval($item->getRatingN())), 1, '.', '');
        
    $slika = "db/slike/" . $item->getPicture();
        
    $msg = "";
    
    if($ratingID == $item->getID())
        $msg = $ratingMsg;
    
    echo "
        <tr><th id='prvi'>Naziv: </th><th>".$item->getName()."</th></tr>
        <tr><td id='prvi'>Kategorija: </td><td>".$item->getCategory()."</td></tr>
        <tr><td id='prvi'></td><td><img src='".$slika."' /></td></tr>
        <tr><td id='prvi'>Opis: </td><td>".$item->getDescription()."</td></tr>
        <tr><td id='prvi'>Cena: </td><td>".$item->getPrice()." €</td></tr>
            ";
        if($anonymous == false){
            echo "
        <tr><td id='prvi'>Ocena strank: ".$rating."</td>
            <td>
                <form method='post' action='index.php?page=browse&action=rate'>
                    <input type='hidden' name='productId' value='".$item->getID()."' />
                    <select name='rating'>
                        <option value='5'>5</option>
                        <option value='4'>4</option>
                        <option value='3'>3</option>
                        <option value='2'>2</option>
                        <option value='1'>1</option>
                    </select>      
                    <input type='submit' value='Oceni' />
                </form>
                ". $msg ."
            </td>
        </tr><tr><td id='prvi'><a id='btn' href='index.php?page=browse&action=add&id=".$item->getID()."'>Dodaj v košarico</a></td></tr>";
        }
    }
    }
    else
        echo "Ni zadetkov";
    ?>
</table>
</div>