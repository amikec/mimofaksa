-- MySQL dump 10.13  Distrib 5.1.58, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: mimofaksa
-- ------------------------------------------------------
-- Server version	5.1.58-1ubuntu1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Izdelek`
--

DROP TABLE IF EXISTS `Izdelek`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Izdelek` (
  `idIzdelek` int(11) NOT NULL AUTO_INCREMENT,
  `nazivIzdelek` varchar(45) COLLATE utf8_slovenian_ci NOT NULL,
  `opisIzdelek` text COLLATE utf8_slovenian_ci NOT NULL,
  `kategorijaIzdelek` varchar(45) COLLATE utf8_slovenian_ci NOT NULL,
  `cenaIzdelek` decimal(10,2) NOT NULL,
  `ocenanIzdelek` int(11) NOT NULL DEFAULT '0',
  `ocenasumIzdelek` decimal(10,1) NOT NULL DEFAULT '0.0',
  `aktivenIzdelek` tinyint(1) NOT NULL DEFAULT '0',
  `slikaIzdelek` varchar(45) COLLATE utf8_slovenian_ci DEFAULT NULL,
  PRIMARY KEY (`idIzdelek`),
  FULLTEXT KEY `searchIzdelek` (`opisIzdelek`,`kategorijaIzdelek`,`nazivIzdelek`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Izdelek`
--

LOCK TABLES `Izdelek` WRITE;
/*!40000 ALTER TABLE `Izdelek` DISABLE KEYS */;
INSERT INTO `Izdelek` VALUES (1,'Prenosnik HP  ProBook 4530s ','HP ProBook 4530s je primeren za vse, ki potrebujejo prenosnik, ki je zmogljiv, odporen in elegantno oblikovan. S 4 GB DDR3 pomnilnika, Intel Core i3 procesorjem in 640 GB trdega diska bo gotovo kos še tako zahtevnim nalogam, ki jih boste želeli opraviti.','računalništvo','639.00',5,'18.0',1,'hpprobook.jpg'),(2,'Monitor LCD 23,6\" LG W2443T','Monitor LG W2443T je LCD monitor, ki je namenjen vsem domačim uporabnikom, ki želijo velik zaslon za urejanje dokumentov in uživanje ob gledanju filmov. Monitor se pohvali z FullHD ločljivostjo, 30.000:1 dinamičnim kontrastnim razmerjem in odzivnim časom 5 ms. ','računalništvo','159.99',2,'9.0',1,'monitorlg.jpg'),(3,'Netbook Lenovo IdeaPad S100 ','Lenovo S100, je predstavnik družine majhnih in lahkih prenosnih računalnikov IdeaPad. Model S100 velja za izjemno tanek (25,5 mm) in lahek (1,2 kg) prenosni računalnik, namenjen uporabnikom, ki potrebujejo predvsem mobilnost, katero zagotavljajo majhne zunanje mere. Prenosniki IdeaPad v eni napravi tako združujejo sodobno tehnologijo, stil, kakovost izdelave in vsesplošno uporabnost.','računalništvo','299.22',2,'10.0',1,'lenovoideapad.jpg'),(4,'Tablični računalnik Archos 7, 8 GB','Archos 7 vam omogoča skoraj vse, kar si lahko zaželite na digitalni tablici. Omogoča vam enostaven dostop do interneta, prilagodljivost ter predvajanje multimedije in še več.','računalništvo','99.99',0,'0.0',1,'archos.jpg'),(5,'Prenosnik Toshiba Satellite L750-1MV ','Prenosnik Toshiba Satellite L750-1MV je pravi poslovni prenosnik opremljen z zmogljivimi komponentami Intel Core i3 procesor, 4 GB pomnilnika, 750 GB disk in odlične tehnologije. ','računalništvo','499.99',0,'0.0',1,'toshibasatellite.jpg'),(6,'Tablični računalnik Asus Eee Pad Slider','Eee Pad Slider je tablica, ki so jo pri Asusu razvili za kar maksimalno udobje pri delu in zabavi. Poganja jo nVidia Tegra 2 procesor in ima zaslon občutljiv na dotik. Svojo vsestranskost izkazuje z Android operacijskim sistemov, 10,1\" LCD zaslonom občutljivim na dotik, podporo Micro SD karticam, mini HDMI vmesnikom, USB režo, spletno kamero in brezžično povezljivostjo. Odlikuje ga Full QWERTY tipkovnica.','računalništvo','459.99',0,'0.0',0,'asuspad.jpg'),(7,'AV sprejemnik Pioneer VSX-521-K','AV sprejemnik Pioneer VSX-521-K se podaja z izredno kakovostnimi komponentami ter 5x 130 W ojačevalnikom, ki naredi zvok resnično najboljši. ','avdio-video','249.98',4,'18.0',1,'pioneerAV.jpg'),(8,'LED LCD TV sprejemnik Samsung UE40D6530 ','Zavidljivo privlačen LED LCD TV sprejemnik Samsung UE40D6530 ponuja velikost diagonale zaslona 101,6 cm, tehnologijo 3D prikazovanja slike, polno HD ločljivost 1920 x 1080 slikovnih pik, vgrajeno brezžično tehnologijo, 3 USB priključke, 4 HDMI vhode, 400 Hz osveževanje slike, funkcijo pretvorbe 2D vsebin v 3D','avdio-video','897.99',0,'0.0',1,'ledsamsung.jpg'),(9,'Plasma TV sprejemnik Panasonic TX-P46G30E','Panasonic TX-P46G30E je Full-HD Plazma TV sprejemnik z odlično kakovostjo slike in naprednimi možnostmi mrežnega povezovanja. VIERA plazma zaslon ima vgrajeno tehnologijo 600Hz Sub-Field Drive, ki skupaj z Inteligent Frame Creation Pro tehnologijo prinaša gladko, izostreno in vedno jasno sliko, ne glede na gibanje, ki prikazuje.','avdio-video','929.00',0,'0.0',1,'plasmapanasonic.jpg'),(10,'LED LCD TV sprejemnik Philips 40PFL5606H','Sprostite se in si privoščite čudovit večer ob televizorju s čudovito sliko z LED-tehnologijo 100 Hz PMR. Sprostite se in uživajte v filmskem večeru v izjemni kakovosti z LED-televizorjem Philips 32PFL5606 serije 5000. Mehanizem Pixel Plus HD zagotavlja jasne in živahne slike Full HD, ki jih dopolnjuje pristen zvok. ','avdio-video','539.00',0,'0.0',1,'lcdphilips.jpg'),(11,'Full HD kamera Sony HDR-CX130S','Kakovostna Sony Full HD videokamera z bliskovnim pomnilnikom je izredno preprosta za uporabo in omogoča visoko kakovost posnetkov in ponuja zmogljiv zoom. Omogoča snemanje v polni visoki ločljivosti na pomnilniško kartico, širokokotni objektiv, 30-kratni optični zoom, fotografije s 3,3 MP, 7,5 cm/3\" LCD, iAUTO in še veliko več. Zraven kamere prejmete še SD kartico kapacitete 8 G.','avdio-video','424.90',2,'8.0',1,'kamerasony.jpg'),(12,'Par zvočnikov Blaupunkt GT Power 65.2 c','Avtozvočniki Blaupunkt GT Power 65.2 c so dvokomponentni zvočniki premera 16 cm. ','avdio-video','66.75',1,'1.0',0,'blaupunkt.jpg'),(13,'Digitalni fotoaparat Canon EOS 600D Digitalni','Tipalo CMOS z 18 milijoni slikovnih pik, možnost snemanja video posnetkov v polni HD ločljivosti 1920 x 1080, 3,7 slike/sekundo, 9 točkovni - široko področni AF, EOS integriran sistem čiščenja, vrtljiv 7,7-cm LCD zaslon s 1.040.000 slikovnimi pikami, DIGIC 4 procesor, Basic+ in ustvarjalni filtri, vgrajen brezžični nadzor bliskavice ... Priložen je objektiv Canon EF-S 18-55mm 3,5-5,6 IS II + objektiv Canon EF 50 1.8','foto','699.00',6,'25.0',1,'canonEOS.jpg'),(14,'Digitalni fotoaparat Samsung WB700','Digitalni fotoaparat Samsung WB700, v mikavni črni barvi, je izjemno tanek, zmogljiv in zelo eleganten kompakten. Ima 18-kratni optični zoom, ločljivost 14,2 MP, 7,6-cm barvni zaslon in druge uporabne lastnosti.','foto','189.32',0,'0.0',1,'fotosamsung.jpg'),(15,'Digitalni fotoaparat Nikon 1 J1 10-30 mm','Vrhunska hitrost v majhnem ohišju, to je Nikon 1 J1. Ponaša se s CMOS senzorjem formata CX ločljivosti 10,1 MP, snemanjem video posnetkov v polni HD ločljivosti, zaporednim fotografiranjem s 60 posnetki na sekundo in inovativnima funkcijama hitre fotografije ter pametnega izbora. ','foto','499.00',0,'0.0',1,'fotonikon.jpg'),(16,'Digitalni foto zaslon Praktica DF 4.7','Digitalni foto okvir z diagonalo zaslona 17,8 cm, ločljivostjo 480 x 234 pik, prikazom slik (JPEG) in podporo SD/SDHC. ','foto','24.90',0,'0.0',0,'fotoframe.jpg'),(17,'Kondenzacijski sušilni stroj Whirlpool AZB 76','Kondenzacijski sušilni stroj Whirlpool AZB 7670 WH ima zmogljivost sušenja 7 kg, odlikuje ga 6th Sense tehnologija, ponaša pa se tudi s funkcijo proti mečkanju perila.','gospodinjstvo','339.00',0,'0.0',1,'whirpoolsusilec.jpg'),(18,'Vgradni pomivalni stroj Bosch SPV40E10EU','Bosch SPV40E10EU je vgradni pomivalni stroj energijskega razreda A+. Ima zmogljivost pomivanja 9 pogrinjkov, 4 različne nastavitve temperature in 4 različne programe pranja posode.','gospodinjstvo','459.42',0,'0.0',1,'boshpomivalni.jpg'),(19,'Kuhinjski robot Gorenje SBR1000B','Kuhinjski robot Gorenje SBR1000B odlikuje 10 hitrosti delovanja z dodatnim Pulse gumbom, ter 12 stopenj moči. Robot ima ogromno različnih nastavkov, snemljive diske, stiskalnik za sok, ter 2 stekleni posodi.','gospodinjstvo','204.90',0,'0.0',1,'gorenjerobot.jpg'),(20,'Robotski sesalnik iRobot Roomba 531','Robotski sesalnik iRobot Roomba 531 izredno pametno ter posledično učinkovito očisti vaša tla. Njegovo močno sesanje in vrteče krtačke očistijo okoli in pod pohištvom, vzdolž sten in vogalov. Samodejno se prilagaja različnim vrstam tal ter se po končanem sesanju vrne na polnilno postajo.','gospodinjstvo','299.90',2,'6.0',0,'irobot.jpg'),(21,'nov artikel','asdfasldujgasdfhkl','foto','123.34',0,'0.0',1,NULL);
/*!40000 ALTER TABLE `Izdelek` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Narocilo`
--

DROP TABLE IF EXISTS `Narocilo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Narocilo` (
  `idNarocilo` int(11) NOT NULL AUTO_INCREMENT,
  `cenaNarocilo` decimal(10,2) NOT NULL,
  `datumNarocilo` datetime NOT NULL,
  `datumPotrjenoNarocilo` datetime DEFAULT NULL,
  `statusNarocilo` int(11) DEFAULT '0',
  `Uporabnik_idUporabnik` int(11) NOT NULL,
  `Naslov_idNaslov` int(11) NOT NULL,
  PRIMARY KEY (`idNarocilo`,`Uporabnik_idUporabnik`,`Naslov_idNaslov`),
  KEY `fk_Narocilo_Uporabnik` (`Uporabnik_idUporabnik`),
  KEY `fk_Narocilo_Naslov` (`Naslov_idNaslov`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Narocilo`
--

LOCK TABLES `Narocilo` WRITE;
/*!40000 ALTER TABLE `Narocilo` DISABLE KEYS */;
INSERT INTO `Narocilo` VALUES (1,'682.70','2011-12-12 13:26:00','2012-01-08 19:58:33',1,5,2),(2,'748.80','2011-12-20 13:00:00',NULL,0,9,6),(3,'798.97','2011-12-27 18:20:00',NULL,0,10,7),(4,'2187.80','2011-12-30 13:42:00',NULL,0,8,5),(5,'1069.92','2012-01-08 20:05:34',NULL,0,5,2);
/*!40000 ALTER TABLE `Narocilo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Naslov`
--

DROP TABLE IF EXISTS `Naslov`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Naslov` (
  `idNaslov` int(11) NOT NULL AUTO_INCREMENT,
  `ulicaNaslov` varchar(45) COLLATE utf8_slovenian_ci DEFAULT NULL,
  `krajNaslov` varchar(45) COLLATE utf8_slovenian_ci DEFAULT NULL,
  `postnastevilkaNaslov` varchar(45) COLLATE utf8_slovenian_ci DEFAULT NULL,
  `drzavaNaslov` varchar(45) COLLATE utf8_slovenian_ci DEFAULT NULL,
  `Uporabnik_idUporabnik` int(11) NOT NULL,
  PRIMARY KEY (`idNaslov`,`Uporabnik_idUporabnik`),
  KEY `fk_Naslov_Uporabnik` (`Uporabnik_idUporabnik`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Naslov`
--

LOCK TABLES `Naslov` WRITE;
/*!40000 ALTER TABLE `Naslov` DISABLE KEYS */;
INSERT INTO `Naslov` VALUES (1,'Testna 35','Test','9999','Test',4),(2,'Prekmurska 3','Ljubljana','1000','Slovenija',5),(3,'Prekmurska 3','Ljubljana','1000','Slovenija',6),(4,'Tržaška 24','Ljubljana','1000','Slovenija',7),(5,'Titova 34','Ljubljana','1000','Slovenija',8),(6,'MariahilferStrasse 34','Wien','1010','Osterreich',9),(7,'Trg 50','Prevalje','2391','Slovenija',10);
/*!40000 ALTER TABLE `Naslov` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Postavka`
--

DROP TABLE IF EXISTS `Postavka`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Postavka` (
  `idPostavka` int(11) NOT NULL AUTO_INCREMENT,
  `kolicinaPostavka` int(45) NOT NULL,
  `Narocilo_idNarocilo` int(11) NOT NULL,
  `Izdelek_idIzdelek` int(11) NOT NULL,
  PRIMARY KEY (`idPostavka`,`Narocilo_idNarocilo`,`Izdelek_idIzdelek`),
  KEY `fk_Postavka_Narocilo` (`Narocilo_idNarocilo`),
  KEY `fk_Postavka_Izdelek` (`Izdelek_idIzdelek`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Postavka`
--

LOCK TABLES `Postavka` WRITE;
/*!40000 ALTER TABLE `Postavka` DISABLE KEYS */;
INSERT INTO `Postavka` VALUES (1,3,3,4),(2,1,3,15),(3,1,1,3),(4,2,1,12),(5,1,2,17),(6,2,2,19),(7,2,4,11),(8,1,4,1),(9,1,4,13),(10,1,1,7),(11,3,5,7),(12,2,5,2);
/*!40000 ALTER TABLE `Postavka` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Uporabnik`
--

DROP TABLE IF EXISTS `Uporabnik`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Uporabnik` (
  `idUporabnik` int(11) NOT NULL AUTO_INCREMENT,
  `imeUporabnik` varchar(45) COLLATE utf8_slovenian_ci NOT NULL,
  `priimekUporabnik` varchar(45) COLLATE utf8_slovenian_ci NOT NULL,
  `emsoUporabnik` varchar(13) COLLATE utf8_slovenian_ci NOT NULL,
  `gesloUporabnik` varchar(20) COLLATE utf8_slovenian_ci NOT NULL,
  `levelUporabnik` int(11) NOT NULL DEFAULT '0',
  `emailUporabnik` varchar(45) COLLATE utf8_slovenian_ci NOT NULL,
  `aktivenUporabnik` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idUporabnik`),
  FULLTEXT KEY `searchUporabnik` (`imeUporabnik`,`priimekUporabnik`,`emailUporabnik`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_slovenian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Uporabnik`
--

LOCK TABLES `Uporabnik` WRITE;
/*!40000 ALTER TABLE `Uporabnik` DISABLE KEYS */;
INSERT INTO `Uporabnik` VALUES (1,'administrator','administrator','administrator','administrator',2,'admin@mimofaksa.com',1),(2,'Marija','Novak','prodajalec','marijanovak',1,'marija.novak@mimofaksa.com',1),(3,'Janez','Medved','prodajalec','janezmedved',1,'janez.medved@mimofaksa.com',1),(4,'Test','Test','1234567890123','test',0,'test@test.test',0),(5,'Janez','Novak','emso','janeznovak',0,'janez@novak.com',1),(6,'Hilda','Novak','emso','hildanovak',0,'hilda@novak.com',1),(7,'France','Arhar','emso','francearhar',0,'france@arhar.com',0),(8,'Gregor','Lovec','emso','gregorlovec',0,'gregor@lovec.com',1),(9,'Micka','Udihar','emso','mickaudihar',0,'micka@udihar.com',0),(10,'Maks','Propeler','emso','makspropeler',0,'maks@propeler.com',0),(11,'Uporabnik','Uporabnik','1234567890123','uporabnik',0,'upo@rabnik.com',0),(12,'New','User','1234567890098','user',0,'asdf@asdf.asdf',0),(13,'Novnov','Uporabnik','1234567890123','Novnov',0,'asdf@asdf.asdf',0),(14,'Novtest','Prodajalec','1234567890123','Nov',1,'asdf@asdf.asdf',0);
/*!40000 ALTER TABLE `Uporabnik` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-01-10 19:43:56
