<?php

if (!isset($_SERVER["HTTPS"])) {
    $url = "https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
    header("Location: " . $url);
}

require_once 'HTML/QuickForm2.php';
require_once 'db/dbConnection.php';

$db = new dbConnection();

$form = new HTML_QuickForm2('registracija','POST',array('action' => 'index.php?page=register'));

$fs = $form->addFieldset('osebniPodatki');
$fs->setLabel('Osebni podatki');

$ime = $fs->addElement('text', 'ime', array('size' => 20))
        ->setLabel('Ime:');
$ime->addRule('required', 'Vnesi ime.');
$ime->addRule('regex', 'Napacen format.', '/^[A-Z][a-z]+$/');

$priimek = $fs->addElement('text', 'priimek', array('size' => 20))
        ->setLabel('Priimek:');
$priimek->addRule('required', 'Vnesi priimek.');
$priimek->addRule('regex', 'Napacen format.', '/^[A-Z][a-z]+$/');

$email = $fs->addElement('text', 'email', array('size' => 20))
        ->setLabel('E-Mail:');
$email->addRule('required', 'Vnesi email.');
$email->addRule('regex', 'Napacen format.', '/^[^0-9][A-z0-9_]+([.][A-z0-9_]+)*[@][A-z0-9_]+([.][A-z0-9_]+)*[.][A-z]{2,4}$/');

$stevilka = $fs->addElement('text', 'emso', array('size' => 13))
        ->setLabel('EMSO:');
$stevilka->addRule('required', 'Vnesi emso.');
$stevilka->addRule('regex','Stevilke, 13 mest.','/^[0-9]{13}$/');

$geslo = $fs->addElement('password', 'geslo', array('size' => 20))
        ->setLabel('Geslo:');
$geslo->addRule('required', 'Vnesi geslo.');
$geslo->addRule('regex','Napacen format.','/^[A-Za-z0-9_ ]+$/');

$geslo2 = $fs->addElement('password', 'geslo2', array('size' => 20))
        ->setLabel('Ponovi geslo:');
$geslo2->addRule('required', 'Vnesi geslo.');
$geslo2->addRule('regex','Napacen format.','/^[A-Za-z0-9_ ]+$/');


$fs->addElement('submit', null, array('value' => 'Registriraj'));

if(isset($_POST['ime'])){
        $form->addDataSource(new HTML_QuickForm2_DataSource_Array(array('ime' => $_POST['ime'],
                'priimek' => $_POST['priimek'],
                'email' => $_POST['email'],
                'emso' => $_POST['emso'],
                'geslo' => $_POST['geslo'],
                'geslo2' => $_POST['geslo2'])));
    
}

$valid1 = false;

if($form->validate() && !isset($_POST['captcha'])){

    $name = $_POST['ime'];
    $priimek = $_POST['priimek'];
    $emso = $_POST['emso'];
    $email = $_POST['email'];
    $pass = $_POST['geslo'];
    $pass2 = $_POST['geslo2'];

    if($pass == $pass2){
        $valid1 = true;
    }else{
        echo "Gesli se ne ujemata";
        echo $form;
    }
        
}else if(!isset($_POST['captcha'])){
    echo $form;
}

if($valid1 == true || (isset($_POST['captcha']) && $_POST['captcha'] < 10)){
        require_once('capt/recaptchalib.php');
        

        echo "<form action='index.php?page=register' method='post'>";
        
        // Get a key from https://www.google.com/recaptcha/admin/create
        $publickey = "6LdWE8wSAAAAAJkZCi99FgW650N78rqSx3diqMjp";
        $privatekey = "6LdWE8wSAAAAAF6gUF91pEO5IYjA_RqgiryzyHaR";
        # the response from reCAPTCHA
        $resp = null;
        # the error code from reCAPTCHA, if any
        $error = null;

        $try = 0;
        
        if(isset($_POST['captcha']))
            $try = $_POST['captcha'] + 1; 
        
        echo "<input type='hidden' name='ime' value='" . $_POST['ime'] . "' />";
        echo "<input type='hidden' name='priimek' value='" . $_POST['priimek'] . "' />";
        echo "<input type='hidden' name='geslo' value='" . $_POST['geslo'] . "' />";
        echo "<input type='hidden' name='geslo2' value='" . $_POST['geslo2'] . "' />";
        echo "<input type='hidden' name='email' value='" . $_POST['email'] . "' />";
        echo "<input type='hidden' name='emso' value='" . $_POST['emso'] . "' />";
        echo "<input type='hidden' name='captcha' value='".$try."' />";
      
        
        echo recaptcha_get_html($publickey, $error);
        echo "<br/>
            <input type='submit' value='Potrdi' />"; 
}else if(isset($_POST['captcha']) && $_POST['captcha'] >= 10){
    echo "Presegli ste število dovoljenih poskusov.";
}


if (isset($_POST["recaptcha_response_field"])) {
        
        $resp = recaptcha_check_answer ($privatekey,
                                        $_SERVER["REMOTE_ADDR"],
                                        $_POST["recaptcha_challenge_field"],
                                        $_POST["recaptcha_response_field"]);

        if ($resp->is_valid) {
                echo "k";
                echo $_POST['ime'], $_POST['priimek'], $_POST['emso'], $_POST['geslo'], $_POST['email'];
                
                $db->createUser($_POST['ime'], $_POST['priimek'], $_POST['emso'], $_POST['geslo'], $_POST['email']);
             
                header("Location: index.php?page=login");
                exit;

        } else {
                echo "<br />Poskusi ponovno.";
                # set the error code so that we can display it
                $error = $resp->error;
        }
}

?>
</form>