\select@language {slovene}
\select@language {slovene}
\contentsline {chapter}{\numberline {1}Uvod}{2}{chapter.1}
\contentsline {chapter}{\numberline {2}Primeri uporabe}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Vloga anonimni odjemalec}{3}{section.2.1}
\contentsline {section}{\numberline {2.2}Vloga prijavljena stranka}{3}{section.2.2}
\contentsline {section}{\numberline {2.3}Vloga prodaja}{4}{section.2.3}
\contentsline {section}{\numberline {2.4}Vloga administrator}{4}{section.2.4}
\contentsline {chapter}{\numberline {3}Uporabni\IeC {\v s}ke pristopne to\IeC {\v c}ke}{5}{chapter.3}
\contentsline {chapter}{\numberline {4}Podatkovni model}{6}{chapter.4}
\contentsline {chapter}{\numberline {5}Varnost sistema}{8}{chapter.5}
\contentsline {chapter}{\numberline {6}Izjava o avtorstvu seminarske naloge}{9}{chapter.6}
\contentsline {chapter}{\numberline {7}Dodatno vzor\IeC {\v c}no poglavje}{12}{chapter.7}
\contentsline {section}{\numberline {7.1}Dodatni vzor\IeC {\v c}ni odsek ena}{12}{section.7.1}
\contentsline {section}{\numberline {7.2}Dodatni vzor\IeC {\v c}ni odsek dva}{12}{section.7.2}
\contentsline {section}{\numberline {7.3}Dodatni vzor\IeC {\v c}ni odsek tri}{12}{section.7.3}
\contentsline {chapter}{\numberline {8}Zaklju\IeC {\v c}ek}{13}{chapter.8}
\contentsline {chapter}{\numberline {9}Literatura}{14}{chapter*.2}
\vspace {15pt}
\contentsline {chapter}{\numberline {A}Naslov dodatka}{15}{appendix.A}
