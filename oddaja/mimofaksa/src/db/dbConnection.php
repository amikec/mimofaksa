<?php

require_once 'User.php';
require_once 'Item.php';
require_once 'Address.php';
require_once 'Basket.php';
require_once 'Order.php';

class dbConnection {

    private $dbh;

    public function __construct() {
        try {
            $this->dbh = new PDO("mysql:host=localhost;sbname=mimofaksa", "root", "ep");
            $this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->dbh->exec("SET NAMES 'utf8';");
        } catch (PDOException $e) {
            echo "Connection failed: $e->getMessage()";
        }
    }

    //USER METHODS

    public function getUserByID($ID) {
        try {
            $stmt = $this->dbh->prepare("SELECT * FROM mimofaksa.Uporabnik WHERE idUporabnik = :id");
            $stmt->bindValue(":id", $ID, PDO::PARAM_INT);
            $stmt->execute();
            $row = $stmt->fetch();

            if ($row == null)
                return null;

            $user = new User($row["idUporabnik"], $row["imeUporabnik"], $row["priimekUporabnik"], $row["emailUporabnik"], $row["levelUporabnik"], $row["emsoUporabnik"], $row["aktivenUporabnik"]);
            return $user;
        } catch (Exception $e) {
            echo "Query failed: " . $e->getMessage();
            return null;
        }
    }

    public function getUserByLogin($mail, $password) {
        try {
            $stmt = $this->dbh->prepare("SELECT * FROM mimofaksa.Uporabnik WHERE emailUporabnik = :email AND gesloUporabnik=:password");
            $stmt->bindValue(":email", $mail);
            $stmt->bindValue(":password", $password);
            $stmt->execute();
            $row = $stmt->fetch();

            if ($row == null)
                return null;

            $user = new User($row["idUporabnik"], $row["imeUporabnik"], $row["priimekUporabnik"], $row["emailUporabnik"], $row["levelUporabnik"], $row["emsoUporabnik"], $row["aktivenUporabnik"]);
            return $user;
        } catch (Exception $e) {
            echo "Query failed: " . $e->getMessage();
            return null;
        }
    }

    public function getUsers() {
        try {
            $stmt = $this->dbh->prepare("SELECT * FROM mimofaksa.Uporabnik");
            $stmt->execute();

            if ($stmt->rowCount() == 0)
                return null;

            $users = array();

            foreach ($stmt->fetchAll() as $row)
                $users[$row["idUporabnik"]] = new User($row["idUporabnik"], $row["imeUporabnik"], $row["priimekUporabnik"], $row["emailUporabnik"], $row["levelUporabnik"], $row["emsoUporabnik"], $row["aktivenUporabnik"]);
            return $users;
        } catch (Exception $e) {
            echo "Query failed: " . $e->getMessage();
            return null;
        }
    }

    public function getUsersByLevel($level) {
        try {
            $stmt = $this->dbh->prepare("SELECT * FROM mimofaksa.Uporabnik WHERE levelUporabnik=:level");
            $stmt->bindValue(":level", $level, PDO::PARAM_INT);
            $stmt->execute();

            if ($stmt->rowCount() == 0)
                return null;

            $users = array();

            foreach ($stmt->fetchAll() as $row)
                $users[$row["idUporabnik"]] = new User($row["idUporabnik"], $row["imeUporabnik"], $row["priimekUporabnik"], $row["emailUporabnik"], $row["levelUporabnik"], $row["emsoUporabnik"], $row["aktivenUporabnik"]);
            return $users;
        } catch (Exception $e) {
            echo "Query failed: " . $e->getMessage();
            return null;
        }
    }

    public function getUsersByActive($active) {
        try {
            $stmt = $this->dbh->prepare("SELECT * FROM mimofaksa.Uporabnik WHERE aktivenUporabnik=:active");
            $stmt->bindValue(":active", $active, PDO::PARAM_INT);
            $stmt->execute();

            if ($stmt->rowCount() == 0)
                return null;

            $users = array();

            foreach ($stmt->fetchAll() as $row)
                $users[$row["idUporabnik"]] = new User($row["idUporabnik"], $row["imeUporabnik"], $row["priimekUporabnik"], $row["emailUporabnik"], $row["levelUporabnik"], $row["emsoUporabnik"], $row["aktivenUporabnik"]);
            return $users;
        } catch (Exception $e) {
            echo "Query failed: " . $e->getMessage();
            return null;
        }
    }

    public function createUser($name, $lastname, $emso, $pass, $email, $level=0, $active=0) {
        try {
            $stmt = $this->dbh->prepare("INSERT INTO mimofaksa.Uporabnik ( imeUporabnik,priimekUporabnik,emsoUporabnik,gesloUporabnik,levelUporabnik,emailUporabnik, aktivenUporabnik) VALUES(:name,:lastname,:emso,:pass,:level,:email,:active)");
            $stmt->bindValue(":name", $name);
            $stmt->bindValue(":lastname", $lastname);
            $stmt->bindValue(":pass", $pass);
            $stmt->bindValue(":email", $email);
            $stmt->bindValue(":emso", $emso);
            $stmt->bindValue(":active", $active, PDO::PARAM_INT);
            $stmt->bindValue(":level", $level, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            echo "Insert failed: " . $e->getMessage();
        }
    }

    public function updateUser($user) {
        try {
            $stmt = $this->dbh->prepare("UPDATE mimofaksa.Uporabnik SET imeUporabnik=:name, priimekUporabnik=:lastname, emsoUporabnik=:emso, emailUporabnik=:email, aktivenUporabnik=:active WHERE idUporabnik=:id");
            $stmt->bindValue(":id", $user->getID(), PDO::PARAM_INT);
            $stmt->bindValue(":name", $user->getName());
            $stmt->bindValue(":lastname", $user->getLastName());
            $stmt->bindValue(":email", $user->getEmail());
            $stmt->bindValue(":emso", $user->getEmso());
            $stmt->bindValue(":active", $user->getActive(), PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            echo "Update failed: " . $e->getMessage();
        }
    }

    public function updateUserPassword($user, $pass) {
        try {
            $stmt = $this->dbh->prepare("UPDATE mimofaksa.Uporabnik SET gesloUporabnik=:pass WHERE idUporabnik=:id");
            $stmt->bindValue(":id", $user->getID(), PDO::PARAM_INT);
            $stmt->bindValue(":pass", $pass);
            $stmt->execute();
        } catch (Exception $e) {
            echo "Update failed: " . $e->getMessage();
        }
    }

    public function deleteUserByID($ID) {
        try {
            $stmt = $this->dbh->prepare("DELETE FROM mimofaksa.Uporabnik WHERE idUporabnik = :id");
            $stmt->bindValue(":id", $ID, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            echo "Deletion failed: " . $e->getMessage();
            return null;
        }
    }

    public function findUsers($string, $level=0) {
        try {
            $stmt = $this->dbh->prepare("SELECT * FROM mimofaksa.Uporabnik WHERE (imeUporabnik LIKE :string OR priimekUporabnik LIKE :string OR emailUporabnik LIKE :string)  AND levelUporabnik=:level");
            //$stmt = $this->dbh->prepare("SELECT * FROM mimofaksa.Uporabnik WHERE MATCH(imeUporabnik,priimekUporabnik,emailUporabnik) AGAINST(:string)  AND levelUporabnik=:level");
            $stmt->bindValue(":string", "%" . $string . "%");
            $stmt->bindValue(":level", $level);
            $stmt->execute();

            if ($stmt->rowCount() == 0)
                return null;

            $users = array();

            foreach ($stmt->fetchAll() as $row)
                $users[$row["idUporabnik"]] = new User($row["idUporabnik"], $row["imeUporabnik"], $row["priimekUporabnik"], $row["emailUporabnik"], $row["levelUporabnik"], $row["emsoUporabnik"], $row["aktivenUporabnik"]);
            return $users;
        } catch (Exception $e) {
            echo "Query failed: " . $e->getMessage();
            return null;
        }
    }

    public function findUsersByActive($string, $active=1, $level=0) {
        try {
            $stmt = $this->dbh->prepare("SELECT * FROM mimofaksa.Uporabnik WHERE (imeUporabnik LIKE :string OR priimekUporabnik LIKE :string OR emailUporabnik LIKE :string) AND aktivenUporabnik=:active  AND levelUporabnik=:level");
            //$stmt = $this->dbh->prepare("SELECT * FROM mimofaksa.Uporabnik WHERE MATCH(imeUporabnik,priimekUporabnik,emailUporabnik) AGAINST(:string)  AND levelUporabnik=:level");
            $stmt->bindValue(":string", "%" . $string . "%");
            $stmt->bindValue(":active", $active);
            $stmt->bindValue(":level", $level);
            $stmt->execute();

            if ($stmt->rowCount() == 0)
                return null;

            $users = array();

            foreach ($stmt->fetchAll() as $row)
                $users[$row["idUporabnik"]] = new User($row["idUporabnik"], $row["imeUporabnik"], $row["priimekUporabnik"], $row["emailUporabnik"], $row["levelUporabnik"], $row["emsoUporabnik"], $row["aktivenUporabnik"]);
            return $users;
        } catch (Exception $e) {
            echo "Query failed: " . $e->getMessage();
            return null;
        }
    }

    //ITEM METHODS

    public function getItemByID($ID) {
        try {
            $stmt = $this->dbh->prepare("SELECT * FROM mimofaksa.Izdelek WHERE idIzdelek= :id");
            $stmt->bindValue(":id", $ID, PDO::PARAM_INT);
            $stmt->execute();
            $row = $stmt->fetch();

            if ($row == null)
                return null;

            $item = new Item($row["idIzdelek"], $row["nazivIzdelek"], $row["opisIzdelek"], $row["kategorijaIzdelek"], $row["cenaIzdelek"], $row["ocenanIzdelek"], $row["ocenasumIzdelek"], $row["aktivenIzdelek"], $row["slikaIzdelek"]);
            return $item;
        } catch (Exception $e) {
            echo "Query failed: " . $e->getMessage();
            return null;
        }
    }

    public function getItems() {
        try {
            $stmt = $this->dbh->prepare("SELECT * FROM mimofaksa.Izdelek");
            $stmt->execute();
            if ($stmt->rowCount() == 0)
                return null;

            $items = array();

            foreach ($stmt->fetchAll() as $row)
                $items[$row["idIzdelek"]] = new Item($row["idIzdelek"], $row["nazivIzdelek"], $row["opisIzdelek"], $row["kategorijaIzdelek"], $row["cenaIzdelek"], $row["ocenanIzdelek"], $row["ocenasumIzdelek"], $row["aktivenIzdelek"], $row["slikaIzdelek"]);
            return $items;
        } catch (Exception $e) {
            echo "Query failed: " . $e->getMessage();
            return null;
        }
    }

    public function getItemsByCategory($category) {
        try {
            $stmt = $this->dbh->prepare("SELECT * FROM mimofaksa.Izdelek WHERE kategorijaIzdelek= :category");
            $stmt->bindValue(":category", $category);
            $stmt->execute();
            if ($stmt->rowCount() == 0)
                return null;

            $items = array();

            foreach ($stmt->fetchAll() as $row)
                $items[$row["idIzdelek"]] = new Item($row["idIzdelek"], $row["nazivIzdelek"], $row["opisIzdelek"], $row["kategorijaIzdelek"], $row["cenaIzdelek"], $row["ocenanIzdelek"], $row["ocenasumIzdelek"], $row["aktivenIzdelek"], $row["slikaIzdelek"]);
            return $items;
        } catch (Exception $e) {
            echo "Query failed: " . $e->getMessage();
            return null;
        }
    }

    public function getItemsByPrice($pricemin=0, $pricemax=9999999, $descending=true, $category=null) {
        try {
            $string = "SELECT * FROM mimofaksa.Izdelek WHERE cenaIzdelek >= :pricemin AND cenaIzdelek <= :pricemax";
            if ($category != null)
                $string = $string . " AND kategorijaIzdelek=:category ";
            if ($descending)
                $string = $string . " ORDER BY cenaIzdelek DESC";
            else
                $string = $string . " ORDER BY cenaIzdelek ASC";

            $stmt = $this->dbh->prepare($string);
            $stmt->bindValue(":pricemin", $pricemin);
            $stmt->bindValue(":pricemax", $pricemax);
            if ($category != null)
                $stmt->bindValue(":category", $category);
            $stmt->execute();
            if ($stmt->rowCount() == 0)
                return null;

            $items = array();

            foreach ($stmt->fetchAll() as $row)
                $items[$row["idIzdelek"]] = new Item($row["idIzdelek"], $row["nazivIzdelek"], $row["opisIzdelek"], $row["kategorijaIzdelek"], $row["cenaIzdelek"], $row["ocenanIzdelek"], $row["ocenasumIzdelek"], $row["aktivenIzdelek"], $row["slikaIzdelek"]);
            return $items;
        } catch (Exception $e) {
            echo "Query failed: " . $e->getMessage();
            return null;
        }
    }

    public function getItemsByRating($ratingmin=0, $ratingmax=5, $descending=true, $category=null) {
        try {
            $string = "SELECT * FROM mimofaksa.Izdelek WHERE (ocenasumIzdelek / ocenanIzdelek ) >= :ratingmin AND (ocenasumIzdelek / ocenanIzdelek ) <= :ratingmax";
            if ($category != null)
                $string = $string . " AND kategorijaIzdelek=:category ";
            if ($descending)
                $string = $string . " ORDER BY ocenasumIzdelek / ocenanIzdelek  DESC";
            else
                $string = $string . " ORDER BY ocenasumIzdelek / ocenanIzdelek  ASC";
            $stmt = $this->dbh->prepare($string);
            if ($category != null)
                $stmt->bindValue(":category", $category);
            $stmt->bindValue(":ratingmin", $ratingmin);
            $stmt->bindValue(":ratingmax", $ratingmax);

            $stmt->execute();
            if ($stmt->rowCount() == 0)
                return null;

            $items = array();

            foreach ($stmt->fetchAll() as $row)
                $items[$row["idIzdelek"]] = new Item($row["idIzdelek"], $row["nazivIzdelek"], $row["opisIzdelek"], $row["kategorijaIzdelek"], $row["cenaIzdelek"], $row["ocenanIzdelek"], $row["ocenasumIzdelek"], $row["aktivenIzdelek"], $row["slikaIzdelek"]);
            return $items;
        } catch (Exception $e) {
            echo "Query failed: " . $e->getMessage();
            return null;
        }
    }

    public function getItemsByActive($active) {
        try {
            $stmt = $this->dbh->prepare("SELECT * FROM mimofaksa.Izdelek WHERE aktivenIzdelek= :active");
            $stmt->bindValue(":active", $active, PDO::PARAM_INT);
            $stmt->execute();
            if ($stmt->rowCount() == 0)
                return null;

            $items = array();

            foreach ($stmt->fetchAll() as $row)
                $items[$row["idIzdelek"]] = new Item($row["idIzdelek"], $row["nazivIzdelek"], $row["opisIzdelek"], $row["kategorijaIzdelek"], $row["cenaIzdelek"], $row["ocenanIzdelek"], $row["ocenasumIzdelek"], $row["aktivenIzdelek"], $row["slikaIzdelek"]);
            return $items;
        } catch (Exception $e) {
            echo "Query failed: " . $e->getMessage();
            return null;
        }
    }

    public function createItem($name, $description, $category, $price, $active, $ratingn=0, $ratingsum=0) {
        try {
            $stmt = $this->dbh->prepare("INSERT INTO mimofaksa.Izdelek ( nazivIzdelek,opisIzdelek,kategorijaIzdelek,cenaIzdelek,ocenanIzdelek,ocenasumIzdelek,aktivenIzdelek) VALUES(:name, :description, :category, :price,:ratingn,:ratingsum,:active)");
            $stmt->bindValue(":name", $name);
            $stmt->bindValue(":category", $category);
            $stmt->bindValue(":price", $price);
            $stmt->bindValue(":description", $description);
            $stmt->bindValue(":active", $active, PDO::PARAM_INT);
            $stmt->bindValue(":ratingn", $ratingn, PDO::PARAM_INT);
            $stmt->bindValue(":ratingsum", $ratingsum, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            echo "Insert failed: " . $e->getMessage();
        }
    }

    public function updateItem($item) {
        try {
            $stmt = $this->dbh->prepare("UPDATE mimofaksa.Izdelek SET nazivIzdelek=:name,opisIzdelek=:description,kategorijaIzdelek=:category,cenaIzdelek=:price,ocenanIzdelek=:ratingn,ocenasumIzdelek=:ratingsum,aktivenIzdelek=:active WHERE idIzdelek=:id");
            $stmt->bindValue(":id", $item->getID(), PDO::PARAM_INT);
            $stmt->bindValue(":name", $item->getName());
            $stmt->bindValue(":description", $item->getDescription());
            $stmt->bindValue(":category", $item->getCategory());
            $stmt->bindValue(":price", $item->getPrice());
            $stmt->bindValue(":ratingn", $item->getRatingN(), PDO::PARAM_INT);
            $stmt->bindValue(":ratingsum", $item->getRatingSum());
            $stmt->bindValue(":active", $item->getActive(), PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            echo "Update failed: " . $e->getMessage();
        }
    }

    public function deleteItemByID($ID) {
        try {
            $stmt = $this->dbh->prepare("DELETE FROM mimofaksa.Izdelek WHERE idIzdelek= :id");
            $stmt->bindValue(":id", $ID, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            echo "Deletion failed: " . $e->getMessage();
            return null;
        }
    }

    public function rateItem($ID, $rating) {
        $item = $this->getItemByID($ID);
        $item->setRatingN($item->getRatingN() + 1);
        $item->setRatingSum($item->getRatingSum() + $rating);
        $this->updateItem($item);
    }

    public function findItems($string, $active=1) {
        try {
            $stmt = $this->dbh->prepare("SELECT * FROM mimofaksa.Izdelek WHERE (nazivIzdelek LIKE :string OR opisIzdelek LIKE :string) AND aktivenIzdelek=:active");
            //$stmt = $this->dbh->prepare("SELECT * FROM mimofaksa.Izdelek WHERE MATCH(nazivIzdelek, opisIzdelek) AGAINST(:string) AND aktivenIzdelek=:active");
            $stmt->bindValue(":string", "%" . $string . "%");
            $stmt->bindValue(":active", $active);
            $stmt->execute();
            if ($stmt->rowCount() == 0)
                return null;

            $items = array();

            foreach ($stmt->fetchAll() as $row)
                $items[$row["idIzdelek"]] = new Item($row["idIzdelek"], $row["nazivIzdelek"], $row["opisIzdelek"], $row["kategorijaIzdelek"], $row["cenaIzdelek"], $row["ocenanIzdelek"], $row["ocenasumIzdelek"], $row["aktivenIzdelek"], $row["slikaIzdelek"]);
            return $items;
        } catch (Exception $e) {
            echo "Query failed: " . $e->getMessage();
            return null;
        }
    }

    //ADDRESS METHODS

    public function getAddressByID($ID) {
        try {
            $stmt = $this->dbh->prepare("SELECT * FROM mimofaksa.Naslov WHERE idNaslov = :id");
            $stmt->bindValue(":id", $ID, PDO::PARAM_INT);
            $stmt->execute();
            $row = $stmt->fetch();
            if ($row == null)
                return null;

            $address = new Address($row["idNaslov"], $row["ulicaNaslov"], $row["krajNaslov"], $row["postnastevilkaNaslov"], $row["drzavaNaslov"], $row["Uporabnik_idUporabnik"]);
            return $address;
        } catch (Exception $e) {
            echo "Query failed: " . $e->getMessage();
            return null;
        }
    }

    public function getAddressesByUserID($ID) {
        try {
            $stmt = $this->dbh->prepare("SELECT * FROM mimofaksa.Naslov WHERE Uporabnik_idUporabnik = :id");
            $stmt->bindValue(":id", $ID, PDO::PARAM_INT);
            $stmt->execute();
            if ($stmt->rowCount() == 0)
                return null;

            $addresses = array();

            foreach ($stmt->fetchAll() as $row)
                $addresses[$row["idNaslov"]] = new Address($row["idNaslov"], $row["ulicaNaslov"], $row["krajNaslov"], $row["postnastevilkaNaslov"], $row["drzavaNaslov"], $row["Uporabnik_idUporabnik"]);
            return $addresses;
        } catch (Exception $e) {
            echo "Query failed: " . $e->getMessage();
            return null;
        }
    }

    public function createAddress($user, $street, $city, $postcode, $country) {
        try {
            $stmt = $this->dbh->prepare("INSERT INTO mimofaksa.Naslov( ulicaNaslov,krajNaslov,postnastevilkaNaslov,drzavaNaslov,Uporabnik_idUporabnik) VALUES (:street,:city,:postcode,:country,:userid)");
            $stmt->bindValue(":street", $street);
            $stmt->bindValue(":city", $city);
            $stmt->bindValue(":postcode", $postcode);
            $stmt->bindValue(":country", $country);
            $stmt->bindValue(":userid", $user->getID(), PDO::PARAM_INT);

            $stmt->execute();
        } catch (Exception $e) {
            echo "Insert failed: " . $e->getMessage();
        }
    }

    public function deleteAddressByID($ID) {
        try {
            $stmt = $this->dbh->prepare("DELETE FROM mimofaksa.Naslov WHERE idNaslov = :id");
            $stmt->bindValue(":id", $ID, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            echo "Deletion failed: " . $e->getMessage();
            return null;
        }
    }

    public function updateAddress($address) {
        try {
            $stmt = $this->dbh->prepare("UPDATE mimofaksa.Naslov SET ulicaNaslov = :street, krajNaslov=:city,postnastevilkaNaslov=:postcode,drzavaNaslov=:country WHERE idNaslov=:id");
            $stmt->bindValue(":street", $address->getStreet());
            $stmt->bindValue(":city", $address->getCity());
            $stmt->bindValue(":postcode", $address->getPostCode());
            $stmt->bindValue(":country", $address->getCountry());
            $stmt->bindValue(":id", $address->getID(), PDO::PARAM_INT);

            $stmt->execute();
        } catch (Exception $e) {
            echo "Update failed: " . $e->getMessage();
        }
    }

    //ORDER METHODS 

    public function createOrder($user, $basket, $address) {
        try {
            $stmt = $this->dbh->prepare("INSERT INTO mimofaksa.Narocilo(cenaNarocilo,datumNarocilo,Uporabnik_idUporabnik, Naslov_idNaslov) VALUES (:orderprice,:orderdate,:userid,:addressid)");
            $stmt->bindValue(":orderprice", $basket->getPrice());
            $stmt->bindValue(":orderdate", date("Y-m-d H:i:s"));
            $stmt->bindValue(":userid", $user->getID(), PDO::PARAM_INT);
            $stmt->bindValue(":addressid", $address->getID(), PDO::PARAM_INT);
            $stmt->execute();

            $orderid = $this->dbh->lastInsertId();

            foreach ($basket->getItems() as $id => $item) {
                $stmt = $this->dbh->prepare("INSERT INTO mimofaksa.Postavka (kolicinaPostavka, Narocilo_idNarocilo, Izdelek_idIzdelek) VALUES (:quantity, :orderid, :itemid)");
                $stmt->bindValue(":quantity", $item->getQuantity(), PDO::PARAM_INT);
                $stmt->bindValue(":orderid", $orderid, PDO::PARAM_INT);
                $stmt->bindValue(":itemid", $item->getID(), PDO::PARAM_INT);
                $stmt->execute();
            }
        } catch (Exception $e) {
            echo "Insert failed: " . $e->getMessage();
        }
    }

    public function updateOrder($order) {
        try {
            $stmt = $this->dbh->prepare("UPDATE mimofaksa.Narocilo SET cenaNarocilo=:price, statusNarocilo=:status, datumNarocilo=:orderdate, datumPotrjenoNarocilo=:orderdatecomplete, Uporabnik_idUporabnik=:userid, Naslov_idNaslov=:addressid WHERE idNarocilo=:id");
            $stmt->bindValue(":id", $order->getID(), PDO::PARAM_INT);
            $stmt->bindValue(":price", $order->getPrice());
            $stmt->bindValue(":status", $order->getStatus(), PDO::PARAM_INT);
            $stmt->bindValue(":orderdate", $order->getDate());
            $stmt->bindValue(":orderdatecomplete", $order->getDateComplete());
            $stmt->bindValue(":userid", $order->getUserID(), PDO::PARAM_INT);
            $stmt->bindValue(":addressid", $order->getAddressID(), PDO::PARAM_INT);
            $stmt->execute();

            $newitems = $order->getItems();

            $items = array();
            $stmt2 = $this->dbh->prepare("SELECT * FROM mimofaksa.Postavka WHERE Narocilo_idNarocilo= :id");
            $stmt2->bindValue(":id", $order->getID(), PDO::PARAM_INT);
            $stmt2->execute();

            foreach ($stmt2->fetchAll() as $row2) {
                $items[$row2["idPostavka"]] = $row2["Izdelek_idIzdelek"];
            }

            foreach ($items as $id => $itemid) {
                if (array_key_exists($itemid, $newitems)) {
                    $stmt2 = $this->dbh->prepare("UPDATE mimofaksa.Postavka SET kolicinaPostavka=:quantity WHERE idPostavka= :id");
                    $stmt2->bindValue(":quantity", $newitems[$itemid]->getQuantity(), PDO::PARAM_INT);
                    $stmt2->bindValue("id", $id, PDO::PARAM_INT);
                    $stmt2->execute();
                    unset($newitems[$itemid]);
                } else {
                    $stmt2 = $this->dbh->prepare("DELETE FROM mimofaksa.Postavka  WHERE idPostavka= :id");
                    $stmt2->bindValue("id", $id, PDO::PARAM_INT);
                    $stmt2->execute();
                }
            }
            foreach ($newitems as $itemid => $item) {
                $stmt = $this->dbh->prepare("INSERT INTO mimofaksa.Postavka (kolicinaPostavka, Narocilo_idNarocilo, Izdelek_idIzdelek) VALUES (:quantity, :orderid, :itemid)");
                $stmt->bindValue(":quantity", $item->getQuantity(), PDO::PARAM_INT);
                $stmt->bindValue(":orderid", $order->getID(), PDO::PARAM_INT);
                $stmt->bindValue(":itemid", $item->getID(), PDO::PARAM_INT);
                $stmt->execute();
            }
        } catch (Exception $e) {
            echo "Update failed: " . $e->getMessage();
        }
    }

    public function changeOrderStatus($order, $status) {
        try {
            $stmt = $this->dbh->prepare("UPDATE mimofaksa.Narocilo SET statusNarocilo=:status, datumPotrjenoNarocilo=:orderdatecomplete WHERE idNarocilo=:id");
            $stmt->bindValue(":id", $order->getID(), PDO::PARAM_INT);
            $stmt->bindValue(":status", $status, PDO::PARAM_INT);
            $stmt->bindValue(":orderdatecomplete", date("Y-m-d H:i:s"));

            $stmt->execute();
        } catch (Exception $e) {
            echo "Update failed: " . $e->getMessage();
        }
    }

    public function getOrderByID($ID) {
        try {
            $stmt = $this->dbh->prepare("SELECT * FROM mimofaksa.Narocilo WHERE idNarocilo= :id");
            $stmt->bindValue(":id", $ID, PDO::PARAM_INT);
            $stmt->execute();
            $row = $stmt->fetch();
            if ($stmt->rowCount() == 0)
                return null;

            $stmt2 = $this->dbh->prepare("SELECT * FROM mimofaksa.Postavka WHERE Narocilo_idNarocilo= :id");
            $stmt2->bindValue(":id", $row["idNarocilo"], PDO::PARAM_INT);
            $stmt2->execute();
            $items = array();
            foreach ($stmt2->fetchAll() as $row2) {
                $item = $this->getItemByID($row2["Izdelek_idIzdelek"]);
                $item->setQuantity($row2["kolicinaPostavka"]);
                $items[$row2["Izdelek_idIzdelek"]] = $item;
            }

            $order = new Order($row["idNarocilo"], $items, $row["statusNarocilo"], $row["datumNarocilo"], $row["datumPotrjenoNarocilo"], $row["Uporabnik_idUporabnik"], $row["Naslov_idNaslov"]);

            return $order;
        } catch (Exception $e) {
            echo "Deletion failed: " . $e->getMessage();
            return null;
        }
    }

    public function getOrdersByUserID($ID) {
        try {
            $stmt = $this->dbh->prepare("SELECT * FROM mimofaksa.Narocilo WHERE Uporabnik_idUporabnik= :userid");
            $stmt->bindValue(":userid", $ID, PDO::PARAM_INT);
            $stmt->execute();

            if ($stmt->rowCount() == 0)
                return null;

            $orders = array();

            foreach ($stmt->fetchAll() as $row) {
                $stmt2 = $this->dbh->prepare("SELECT * FROM mimofaksa.Postavka WHERE Narocilo_idNarocilo= :id");
                $stmt2->bindValue(":id", $row["idNarocilo"], PDO::PARAM_INT);
                $stmt2->execute();
                $items = array();
                foreach ($stmt2->fetchAll() as $row2) {
                    $item = $this->getItemByID($row2["Izdelek_idIzdelek"]);
                    $item->setQuantity($row2["kolicinaPostavka"]);
                    $items[$row2["Izdelek_idIzdelek"]] = $item;
                }

                $orders[$row["idNarocilo"]] = new Order($row["idNarocilo"], $items, $row["statusNarocilo"], $row["datumNarocilo"], $row["datumPotrjenoNarocilo"], $row["Uporabnik_idUporabnik"], $row["Naslov_idNaslov"]);
            }
            return $orders;
        } catch (Exception $e) {
            echo "Deletion failed: " . $e->getMessage();
            return null;
        }
    }
    
        public function getOrders() {
        try {
            $stmt = $this->dbh->prepare("SELECT * FROM mimofaksa.Narocilo");
            $stmt->execute();

            if ($stmt->rowCount() == 0)
                return null;

            $orders = array();

            foreach ($stmt->fetchAll() as $row) {
                $stmt2 = $this->dbh->prepare("SELECT * FROM mimofaksa.Postavka WHERE Narocilo_idNarocilo= :id");
                $stmt2->bindValue(":id", $row["idNarocilo"], PDO::PARAM_INT);
                $stmt2->execute();
                $items = array();
                foreach ($stmt2->fetchAll() as $row2) {
                    $item = $this->getItemByID($row2["Izdelek_idIzdelek"]);
                    $item->setQuantity($row2["kolicinaPostavka"]);
                    $items[$row2["Izdelek_idIzdelek"]] = $item;
                }

                $orders[$row["idNarocilo"]] = new Order($row["idNarocilo"], $items, $row["statusNarocilo"], $row["datumNarocilo"], $row["datumPotrjenoNarocilo"], $row["Uporabnik_idUporabnik"], $row["Naslov_idNaslov"]);
            }
            return $orders;
        } catch (Exception $e) {
            echo "Deletion failed: " . $e->getMessage();
            return null;
        }
    }

    public function getOrdersByStatus($status) {
        try {
            $stmt = $this->dbh->prepare("SELECT * FROM mimofaksa.Narocilo WHERE statusNarocilo= :status");
            $stmt->bindValue(":status", $status, PDO::PARAM_INT);
            $stmt->execute();

            if ($stmt->rowCount() == 0)
                return null;

            $orders = array();

            foreach ($stmt->fetchAll() as $row) {
                $stmt2 = $this->dbh->prepare("SELECT * FROM mimofaksa.Postavka WHERE Narocilo_idNarocilo= :id");
                $stmt2->bindValue(":id", $row["idNarocilo"], PDO::PARAM_INT);
                $stmt2->execute();
                $items = array();
                foreach ($stmt2->fetchAll() as $row2) {
                    $item = $this->getItemByID($row2["Izdelek_idIzdelek"]);
                    $item->setQuantity($row2["kolicinaPostavka"]);
                    $items[$row2["Izdelek_idIzdelek"]] = $item;
                }

                $orders[$row["idNarocilo"]] = new Order($row["idNarocilo"], $items, $row["statusNarocilo"], $row["datumNarocilo"], $row["datumPotrjenoNarocilo"], $row["Uporabnik_idUporabnik"], $row["Naslov_idNaslov"]);

                unset($items);
            }
            return $orders;
        } catch (Exception $e) {
            echo "Deletion failed: " . $e->getMessage();
            return null;
        }
    }

    public function deleteOrderByID($ID) {
        try {
            $stmt = $this->dbh->prepare("DELETE FROM mimofaksa.Narocilo WHERE idNarocilo= :id");
            $stmt->bindValue(":id", $ID, PDO::PARAM_INT);
            $stmt->execute();

            $stmt = $this->dbh->prepare("DELETE FROM mimofaksa.Postavka WHERE Narocilo_idNarocilo= :id");
            $stmt->bindValue(":id", $ID, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            echo "Deletion failed: " . $e->getMessage();
            return null;
        }
    }

}

?>
