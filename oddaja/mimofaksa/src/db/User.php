<?php

class User {

    private $userID;
    private $userName;
    private $userLastName;
    private $userEmail;
    private $userLevel;
    private $userEmso;
    private $userActive;

    //private $userAddresses

    public function __construct($ID, $name, $lastname, $email, $level, $emso, $active) {
        $this->userID = $ID;
        $this->userName = $name;
        $this->userLastName = $lastname;
        $this->userEmail = $email;
        $this->userLevel = $level;
        $this->userEmso = $emso;
        $this->userActive = $active;
    }

    public function getID() {
        return $this->userID;
    }

    public function getName() {
        return $this->userName;
    }

    public function getLastName() {
        return $this->userLastName;
    }

    public function getEmail() {
        return $this->userEmail;
    }

    public function getLevel() {
        return $this->userLevel;
    }

    public function getEmso() {
        return $this->userEmso;
    }

    public function setName($name) {
        $this->userName = $name;
    }

    public function setLastName($lastname) {
        $this->userLastName = $lastname;
    }

    public function setLevel($level) {
        $this->userLevel = $level;
    }

    public function setEmail($email) {
        $this->userEmail = $email;
    }

    public function setEmso($emso) {
        $this->userEmso = $emso;
    }

    public function getActive() {
        return $this->userActive;
    }

    public function setActive($userActive) {
        $this->userActive = $userActive;
    }

}

?>
