<?php

class Item {

    private $itemID;
    private $itemName;
    private $itemDescription;
    private $itemCategory;
    private $itemPrice;
    private $itemRatingN;
    private $itemRatingSum;
    private $itemActive;
    private $itemQuantity;
    private $itemPicture;

    public function __construct($ID, $name, $description, $category, $price, $ratingN, $ratingSum, $active, $picture) {
        $this->itemID = $ID;
        $this->itemName = $name;
        $this->itemDescription = $description;
        $this->itemCategory = $category;
        $this->itemPrice = $price;
        $this->itemRatingN = $ratingN;
        $this->itemRatingSum = $ratingSum;
        $this->itemActive = $active;

        $this->itemPicture = $picture;
    }

    public function getID() {
        return $this->itemID;
    }

    public function getName() {
        return $this->itemName;
    }

    public function getDescription() {
        return $this->itemDescription;
    }

    public function getPrice() {
        return $this->itemPrice;
    }

    public function getCategory() {
        return $this->itemCategory;
    }

    public function getRatingN() {
        return $this->itemRatingN;
    }

    public function getRatingSum() {
        return $this->itemRatingSum;
    }

    public function getActive() {
        return $this->itemActive;
    }

    public function getPicture() {
        return $this->itemPicture;
    }

    public function setID($itemID) {
        $this->itemID = $itemID;
    }

    public function setName($itemName) {
        $this->itemName = $itemName;
    }

    public function setDescription($itemDescription) {
        $this->itemDescription = $itemDescription;
    }

    public function setCategory($itemCategory) {
        $this->itemCategory = $itemCategory;
    }

    public function setPrice($itemPrice) {
        $this->itemPrice = $itemPrice;
    }

    public function setRatingN($itemRatingN) {
        $this->itemRatingN = $itemRatingN;
    }

    public function setRatingSum($itemRatingSum) {
        $this->itemRatingSum = $itemRatingSum;
    }

    public function setActive($itemActive) {
        $this->itemActive = $itemActive;
    }

    public function getQuantity() {
        return $this->itemQuantity;
    }

    public function setQuantity($itemQuantity) {
        $this->itemQuantity = $itemQuantity;
    }

    public function addQuantity($itemQuantity) {
        $this->itemQuantity+= $itemQuantity;
    }

}

?>
