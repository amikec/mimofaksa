
<form action="index.php?page=customer" method="post" id="search" name="search">
    <input type="text" id="search" name="search" />

    <input type="radio" name="active" value="yes" />Aktiven
    <input type="radio" name="active" value="no" /> Neaktiven

    <input type="submit" value="Išči" /><a id="btn" href="index.php?page=customer&action=add">Dodaj uporabnika</a>
</form>
<?php
$db = new dbConnection();
require_once 'HTML/QuickForm2.php';
$listUsers = true;
$loc = "index.php?page=customer&action=create";
if (isset($_GET['id'])) {
    $user = $db->getUserByID($_GET['id']);
    $loc = "index.php?page=customer&action=change";
}



$form = new HTML_QuickForm2('dodajanjeProfila', 'POST', array('action' => $loc));

$fs = $form->addFieldset('osebniPodatki');
$fs->setLabel('Osebni podatki');

$ime = $fs->addElement('text', 'name', array('size' => 20))
        ->setLabel('Ime:');
$ime->addRule('required', 'Vnesi ime.');
$ime->addRule('regex', 'Napacen format.', '/^[A-Z][a-z]+$/');

$priimek = $fs->addElement('text', 'surname', array('size' => 20))
        ->setLabel('Priimek:');
$priimek->addRule('required', 'Vnesi priimek.');
$priimek->addRule('regex', 'Napacen format.', '/^[A-Z][a-z]+$/');

$email = $fs->addElement('text', 'email', array('size' => 20))
        ->setLabel('E-Mail:');
$email->addRule('required', 'Vnesi email.');
$email->addRule('regex', 'Napacen format.', '/^[^0-9][A-z0-9_]+([.][A-z0-9_]+)*[@][A-z0-9_]+([.][A-z0-9_]+)*[.][A-z]{2,4}$/');

$emso = $fs->addElement('text', 'emso', array('size' => 13))
        ->setLabel('Emso');
$emso->addRule('required', 'Vnesi emso.');
$emso->addRule('regex', 'Napacen format.', '/^[0-9]{13}$/');

$fs->addElement('submit', null, array('value' => 'Shrani'));

$form2 = new HTML_QuickForm2('urejanjeProfila', 'POST', array('action' => $loc));

$fs2 = $form2->addFieldset('osebniPodatki2');
$fs2->setLabel('Osebni podatki');

$id = 0;
if(isset($_GET['id']))
    $id = $_GET['id'];

$id2 = $fs2->addElement('hidden', 'id',  array('size' => 20));

$ime2 = $fs2->addElement('text', 'name', array('size' => 20))
        ->setLabel('Ime:');
$ime2->addRule('required', 'Vnesi ime.');
$ime2->addRule('regex', 'Napacen format.', '/^[A-Z][a-z]+$/');

$priimek2 = $fs2->addElement('text', 'surname', array('size' => 20))
        ->setLabel('Priimek:');
$priimek2->addRule('required', 'Vnesi priimek.');
$priimek2->addRule('regex', 'Napacen format.', '/^[A-Z][a-z]+$/');

$email2 = $fs2->addElement('text', 'email', array('size' => 20))
        ->setLabel('E-Mail:');
$email2->addRule('required', 'Vnesi email.');
$email2->addRule('regex', 'Napacen format.', '/^[^0-9][A-z0-9_]+([.][A-z0-9_]+)*[@][A-z0-9_]+([.][A-z0-9_]+)*[.][A-z]{2,4}$/');

$emso2 = $fs2->addElement('text', 'emso', array('size' => 13))
        ->setLabel('Emso');
$emso2->addRule('required', 'Vnesi emso.');
$emso2->addRule('regex', 'Napacen format.', '/^[0-9]{13}$/');

$fs2->addElement('submit', null, array('value' => 'Shrani'));

$action = "";

if (isset($_GET['action'])) {

    if ($_GET['action'] == "activate") {
        $id = $_GET['id'];
        $user = $db->getUserByID($id);
        $user->setActive(1);
        $db->updateUser($user);
    } else if ($_GET['action'] == "deactivate") {
        $id = $_GET['id'];
        $user = $db->getUserByID($id);
        $user->setActive(0);
        $db->updateUser($user);
    } else if ($_GET['action'] == "edit") {
        $action = "edit";
    } else if ($_GET['action'] == "add") {
        $action = "add";
    } else if ($_GET['action'] == "change") {
        if($form2->validate()){

            $id = $_POST['id'];

            $user = $db->getUserByID($id);
            $user->setName($_POST['name']);
            $user->setLastName($_POST['surname']);
            $user->setEmail($_POST['email']);
            $user->setEmso($_POST['emso']);
            //$user->setActive($_POST['active']);

            $db->updateUser($user);
        }else{
            $listUsers = false;
            echo $form2;
        }
    } else if ($_GET['action'] == "create") {
        if($form->validate()){
            $geslo = $_POST['name'].$_POST['surname'];
            $db->createUser($_POST['name'], $_POST['surname'], $_POST['emso'], $geslo, $_POST['email'], 0);
        }else{
            $listUsers = false;
            echo $form;
        }
    }
    echo $_GET['action'];
}

$users = $db->getUsersByLevel(0);

$term = "";
$active = 1;

if (isset($_POST['search'])) {
    $term = $_POST['search'];
    $users = $db->findUsers($term);
}

if (isset($_POST['active'])) {
    $active = $_POST['active'];
    $users = $db->findUsersByActive($term, $active);
}

if ($action == "edit") {
    $form2->addDataSource(new HTML_QuickForm2_DataSource_Array(array('surname' => $user->getLastname(),
                'name' => $user->getName(),
                'email' => $user->getEmail(),
                'emso' => $user->getEmso(),
                'id' => $_GET['id'])));
    echo $form2;
} else if ($action == "add") {
    echo $form;
} else {
if($listUsers){
echo "<table class='custom'>";
        echo "<tr><th>Ime in priimek</th><th>Aktiven</th><th>Email</th><th></th><th></th></tr>";
        if($users != null){
            foreach ($users as $user) {
                $active = "yes";

                if ($user->getActive() == 0)
                    $active = "no";

                echo "<tr><td>" . $user->getName() . " " . $user->getLastName() . "</td>
            <td>" . $active . "</td>
            <td>" . $user->getEmail() . "</td>";
                if ($active == "no")
                    echo "<td><a id='btn' href='index.php?page=customer&action=activate&id=" . $user->getID() . "'>Aktiviraj</a></td>";
                else
                    echo "<td><a id='btn' href='index.php?page=customer&action=deactivate&id=" . $user->getID() . "'>Deaktiviraj</a></td>";
                echo "<td><a id='btn' href='index.php?page=customer&action=edit&id=" . $user->getID() . "'>Uredi</a></td>";
                echo "</tr>";
            }
        }
    }

   
echo "</table>";
}

 ?>