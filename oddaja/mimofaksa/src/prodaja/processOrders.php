<?php

$action = "browse";

$db = new dbConnection();

if(isset($_GET['action'])){
    $action = $_GET['action'];
}

if($action == "confirm"){
    $order = $db->getOrderById($_GET['id']);
    $order = $db->changeOrderStatus($order, 1);
    
    $action = "browse";
}else if($action == "reject"){
    $order = $db->getOrderById($_GET['id']);
    $order = $db->changeOrderStatus($order, -1);
    $action = "browse";
}else if($action == "storno"){
    $order = $db->getOrderById($_GET['id']);
    $order = $db->changeOrderStatus($order, -2);
    $action = "browse";
}


$orders = $db->getOrders();

if($action == "browse"){
    
    
    foreach($orders as $order){
        $status = "odobreno";
        $items = $order->getItems();
        if($order->getStatus() == 0)
            $status = "v čakanju";
        else if($order->getStatus() == -1)
            $status = "zavrnjeno";
        else if($order->getStatus() == -2)
            $status = "stornirano";
        
            echo "<p><table class='custom'>";
        echo "<tr><td colspan='3'>Datum: ".$order->getDate()."</td></tr>";
        echo "<tr><td colspan='3'>Status: ".$status."</td></tr>";

        echo "<tr>";
        echo "<td><a id='btn' href='index.php?page=orders&action=details&id=".$order->getID()."'>Podrobno</a></td>";
        if($order->getStatus() == 0){
            echo "<td><a id='btn' href='index.php?page=orders&action=confirm&id=".$order->getID()."'>Potrdi</a></td>";
            echo "<td><a id='btn' href='index.php?page=orders&action=reject&id=".$order->getID()."'>Zavrni</a></td>";
        }else{
            echo "<td></td><td></td>";
        }
        echo "<td><a id='btn' href='index.php?page=orders&action=storno&id=".$order->getID()."'>Storniraj</a></td></tr>";
        echo "</table></p>";
    }   
    

}else if($action == "details"){
        $order = $db->getOrderByID($_GET['id']); 
        $items = $order->getItems();
        $address = $db->getAddressByID($order->getAddressID());
        $user = $db->getUserByID($order->getUserID());
        
        echo "Datum naročila: ". $order->getDate();
        
        echo "<table>
              <tr><td>Podatki o kupcu:</td><td></td><tr>
              <tr><td></td><td>". $user->getName(). " " . $user->getLastName() . "</td></tr>
              <tr><td></td><td>". $user->getEmail() . "</td></tr>  
              <tr><td></td><td>". $address->getStreet()."</td></tr>
              <tr><td></td><td>". $address->getCity(). " " . $address->getPostCode() . "</td></tr>
              <tr><td></td><td>". $address->getCountry()."</td></tr>    
              </table>";
        
        echo "Items:";
        echo "<table>";
        echo "<tr><th>Naziv:</th><th>Kolicina:</th><th>Cena:</th></tr>";
        if($items != null){ 
            foreach($items as $itemID=>$item){
                echo "<tr><td> ".$item->getName()."</td>";
                echo "<td>".$item->getQuantity()."</td>";
                echo "<td>".$item->getPrice() * $item->getQuantity()."</td></tr>";
            }
            echo "<tr><td></td><td>Skupno:</td><td> " . $order->getPrice() ."</td><td></td><tr>";
        }else{
            echo "<tr><td>Naročilo je prazno!</td></tr>";
        }
        echo "<tr><td><a id='btn' href='index.php?page=orders&action=browse'>Nazaj</a></td>
                  <td><a id='btn' href='index.php?page=orders&action=confirm&id=".$order->getID()."'>Potrdi</a></td>
                  <td><a id='btn' href='index.php?page=order&action=reject&id=".$order->getID()."'>Storniraj</a></td></tr>";
        echo "</table>";
}
?>