<?php

if (!isset($_SERVER["HTTPS"])) {
    $url = "https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
    header("Location: " . $url);
}

require_once 'HTML/QuickForm2.php';

$form = new HTML_QuickForm2('prijava','POST',array('action' => 'index.php?page=login'));
 
$fs = $form->addFieldset('prijavniPodatki');
$fs->setLabel('Prijava');

$upIme = $fs->addElement('text', 'upIme', array('size' => 20))
        ->setLabel('Uporabniško ime:');
$upIme->addRule('required', 'Vnesi uporabniško ime (email).');
$upIme->addRule('regex', 'Napačen format.', '/^[^0-9][A-z0-9_]+([.][A-z0-9_]+)*[@][A-z0-9_]+([.][A-z0-9_]+)*[.][A-z]{2,4}$/');

$geslo = $fs->addElement('password', 'geslo', array('size' => 20))
         ->setLabel('Geslo');
$geslo->addRule('required', 'Vnesi geslo.');
$geslo->addRule('regex','Napačen format.', '/^[a-zA-Z0-9_ ]+$/');
$fs->addElement('submit', null, array('value' => 'Prijavi'));

require_once 'db/dbConnection.php';

if ($form->validate()){
    $db = new dbConnection();
    $user = $db->getUserByLogin($_POST['upIme'], $_POST['geslo']);
    
    if($user != null){
        $_SESSION['user'] = serialize($user);
        
        if($user ->getLevel() == 1){
            header("Location: prodaja/index.php");
        }else if($user->getLevel() == 2){
            header("Location: adm/index.php");
        }else{
            header("Location: index.php?page=introduction");
        }
        exit;
    }else{
        echo "Prijava ni uspela.";
        echo $form;
    }
    
}else{
    echo $form;
}
?>
