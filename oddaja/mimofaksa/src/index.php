<?php 
session_start();

require_once 'db/Basket.php';
require_once 'db/Item.php';
require_once 'db/User.php';
require_once 'db/dbConnection.php';
require_once 'db/Address.php';

if(isset($_GET['action']) && $_GET['action'] == 'rate'){
    setcookie("hasVoted", $_POST['productId'], time()+60*60*24*6);
}

$page = "";

$anonymous = false;
$loginMode = false;

if(isset($_GET['page'])){
    $page = $_GET['page'];
}

if(!isset($_SESSION['user'])){
    $anonymous = true;
}

if(!isset($_SESSION['basket'])){        
    $basket = new Basket();

    $_SESSION['basket'] = serialize($basket);
}

if($page == "login" || $page == "register"){
    $loginMode = true;
}

?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link  rel="stylesheet" href="style.css" type="text/css" />
        <title></title>
    </head>
    <body>
        <table>
            <!-- header -->
            <tr>
                <td class="headerMain" colspan="2">
                    <?php //include 'header.php'
                       
                    ?> 
                </td>
            </tr>
            <!-- center content -->
            <tr>
                <td class ="leftColumn" >
                    
                       <table>
                        <?php if($anonymous == false) {?> 
                        <tr><td><a href="index.php?page=pwd">Spremeni geslo</a></td></tr>
                        <tr><td><a href="index.php?page=history">Zgodovina nakupov</a></td></tr>
                        <tr><td><a href="index.php?page=basket">Košarica</a></td></tr>
                        <tr><td><a href="index.php?page=logout">Odjava</a></td></tr>

                        <?php } ?>
                        <tr><td>
                        
                        <form method="post" id="searchForm" action="index.php?page=browse">
                            <input type="text" id="searchTerm" name="searchTerm" />
                            <input type="submit" maxlength="40" value="Isci" />

                        </form>
                        </td></tr><tr><td>
                        <ul>
                            <li><a href="index.php?page=browse&category=računalništvo">Računalništvo</a></li>
                            <li><a href="index.php?page=browse&category=avdio-video">Avdio-Video</a></li>
                            <li><a href="index.php?page=browse&category=foto">Foto</a></li>
                            <li><a href="index.php?page=browse&category=gospodinjstvo">Gospodinjstvo</a></li>
                        </ul></td></tr>
                     

                    <?php if($page == "login"){ ?>

                        <tr><td><a href="index.php?page=register">Registracija</a></td></tr>

                    <?php }else if($page == "register" || $anonymous){ ?>
                        <tr><td><a href="index.php?page=login">Vpis</a></td></tr>
                    
                    <?php } ?>
                    </table>
                </td>
                <td class ="content" >
                    <?php 
                        switch($page){
                            case "logout":
                                session_destroy();  
                                header("Location: index.php");
                                exit;
                                break;
                            case "history":
                                include 'purchaseHistory.php';
                                break;
                            case "basket":
                                include 'kosarica.php'; 
                                break;
                            case "browse":
                                include 'browseItems.php';
                                break;
                            case "editProfile":
                               include 'editProfile.php';
                                break;
                            case "address":
                                include 'addAddress.php';
                                break;
                            case "login":
                                include 'userLogin.php';
                                break;
                            case "register":
                                include 'userRegistration.php';
                                break;
                            case "pwd":
                                include 'changePwd.php';
                                break;
                            default:
                                include 'introduction.php';
                                break;
                        }
                    ?>
                </td>
            </tr>
            <!-- footer -->
            <tr>
                <td class="kfooterMain" colspan="2">
                    <?php //include 'footer.php' ?> 
                </td>
            </tr>
        </table>
    </body>
</html>
